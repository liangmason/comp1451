/**
 * 
 */
package ca.bcit.comp1451.lab1b;

/**
 * Test class for person
 * @author Mason Liang
 * @version 1.0
 *
 */
public class Test {

	public static void main(String[] args) {

		if(args.length != 4) {
			System.out.println("wrong number of arguments");
		}else {
			Person p = new Person(args[0], args[1], Integer.parseInt(args[2]));
			p.x = Double.parseDouble(args[3]); 
			System.out.println(p.getFirstName());
			System.out.println(p.getLastName());
			System.out.println(p.getYearOfBirth());
			System.out.println(p.x);
		
		}
	}		
}
