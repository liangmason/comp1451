package ca.bcit.comp1451.lesson6.jasonharrison;

public class Car {
	private String color;
	private String make;
	private String model;
	private int    year;
	
	public Car(String color, String make, String model, int year) {
		super();
		this.color = color;
		this.make = make;
		this.model = model;
		this.year = year;
	}
	
	
	@Override
	public int hashCode() {
		return 100;
	}
	
	
	@Override
	public boolean equals(Object that) {
		if(that == null) {
			return false;
		}
		
		if(this == that) {
			return true;
		}
		
		if(!(that instanceof Car)) {
			return false;
		}
		
		Car c = (Car)that;
		
		if((this.make.equals(c.make)) &&
		   (this.model.equals(c.model)) &&
		   (this.year == c.year)) {
			return true;
		}else {
			return false;
		}
	}
}
