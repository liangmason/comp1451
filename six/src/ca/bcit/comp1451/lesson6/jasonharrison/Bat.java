package ca.bcit.comp1451.lesson6.jasonharrison;

public class Bat extends Mammal{

	@Override
	public void move() {
		System.out.println("fly");
	}
	
	@Override
	public void speak() {
		System.out.println("click");
	}
}
