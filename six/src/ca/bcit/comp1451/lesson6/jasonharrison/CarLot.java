package ca.bcit.comp1451.lesson6.jasonharrison;

public class CarLot {

	public static void main(String[] args) {
		Car c1 = new Car("red", "dodge", "hellcat", 2018);
		Car c2 = new Car("blue", "dodge", "hellcat", 2018);
		Car c3 = new Car("red", "dodge", "hellcat", 2018);
		Car c4 = new Car("red", "dodge", "hellcat", 2017);

		System.out.println(c1.equals(c2));
		System.out.println(c1.equals(c3));
		System.out.println(c3.equals(c4));
		
	}

}
