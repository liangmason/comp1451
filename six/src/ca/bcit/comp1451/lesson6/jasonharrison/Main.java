package ca.bcit.comp1451.lesson6.jasonharrison;

public class Main {

	public static void main(String[] args) {
		Mammal[] mammals = new Mammal[7];
		
		mammals[0] = new Dog("fido", 2010);
		mammals[1] = new Chihuahua("rosie", 2008);
		mammals[2] = new Dog("rex", 2018);
		mammals[3] = new Mammal();
		mammals[4] = new Bat();
		mammals[5] = new Dolphin();
		mammals[6] = new Squirrel();
				
		for(Mammal m : mammals) {
			m.move();
			m.speak();
			System.out.println(m);
		}
		
		Dog d1 = new Dog("fido", 2015);
		Dog d2 = new Dog("rex", 2014);
		Dog d3 = new Dog("spot", 2017);
		
		System.out.println(d1.equals(d2));
		System.out.println(d1.equals(d1));
		System.out.println(d1.equals(d3));

		
	}

}
