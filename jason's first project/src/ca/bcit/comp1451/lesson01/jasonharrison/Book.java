package ca.bcit.comp1451.lesson01.jasonharrison;

public class Book {
	private String title;
	private String authorLastName;
	private int    yearPublished;
	
	/**
	 * @param title
	 * @param authorLastName
	 * @param yearPublished
	 */
	public Book(String title, String authorLastName, int yearPublished) {
		super();
		this.title = title;
		this.authorLastName = authorLastName;
		this.yearPublished = yearPublished;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @return the authorLastName
	 */
	public String getAuthorLastName() {
		return authorLastName;
	}
	
	/**
	 * @param authorLastName the authorLastName to set
	 */
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
	/**
	 * @return the yearPublished
	 */
	public int getYearPublished() {
		return yearPublished;
	}
	
	/**
	 * @param yearPublished the yearPublished to set
	 */
	public void setYearPublished(int yearPublished) {
		this.yearPublished = yearPublished;
	}
}
