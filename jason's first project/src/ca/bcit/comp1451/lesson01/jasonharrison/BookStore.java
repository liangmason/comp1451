package ca.bcit.comp1451.lesson01.jasonharrison;

public class BookStore {
	private Book[] books;

	int whatever;
	public int z = 40;

	public BookStore() {
		books = new Book[4];
		
		whatever = 99;
		
		books[0] = new Book("harry potter", 
				            "rowling", 1997);
		
		books[1] = null;
		
		books[2] = new Book("it", "king", 1985);
		
		books[3] = new Book(null, null, 2000);
	}
	
	public void displayAllTitlesInUpperCase() {
		if(books == null) {
			System.out.println("no books");
			return;
		}
		
		for(Book book : books) {
			if(book == null) {
				System.out.println("<no book>");
			}else if(book.getTitle() == null){
				System.out.println("<no title>");
			}else {
				System.out.println(book.getTitle().toUpperCase());								
			}
		}
	}
	
}
