package ca.bcit.comp1451.lesson01.jasonharrison;

import java.util.ArrayList;
import java.util.Iterator;

public class Library {
	private ArrayList<Book> books;

	/**
	 * 
	 */
	public Library() {
		
		books = new ArrayList<>();
		
		books.add(new Book("harry potter", "rowling", 1997));
		books.add(null);
		books.add(new Book(null, null, 2000));
		books.add(new Book("it", "king", 1985));
	}
	
	
	public void displayAllTitlesInUpperCase() {
		
		if((books == null) || (books.size() == 0)) {
			System.out.println("no books");
			return;
		}
		
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b == null) {
				System.out.println("no book here");				
			}else if(b.getTitle() == null) {
				System.out.println("no title");
			}else {
				System.out.println(b.getTitle().toUpperCase());
			}
		}
	}
	
	
}
