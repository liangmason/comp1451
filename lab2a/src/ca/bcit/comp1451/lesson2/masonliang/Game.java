package ca.bcit.comp1451.lesson2.masonliang;

import java.util.Random;
import java.util.Scanner;

public class Game {
	private static final int MAX_GAMES = 5; 
	private static final int BIRTHDAY_MIN_YEAR = 1900; 
	private static final int BIRTHDAY_MAX_YEAR = 2019; // it is +1 
	
	/**
	 * 
	 * @return a random date between 1600 and 2199
	 */
	public static Date getRandomDate() {
		Random r = new Random();
		int randomYear  =  r.nextInt(Date.MAX_YEAR - Date.MIN_YEAR) + Date.MIN_YEAR;
		int randomMonth =  r.nextInt(Date.MAX_MONTH)  + Date.ONE;
		int randomDay   =  r.nextInt(Date.MAX_DAY) 	 + Date.ONE;
		
		Date randomDate = new Date(randomYear, randomMonth, randomDay);
		return randomDate;
	}
	
	/*
	 * Guess the date game
	 */
	public static void guessTheDate() {
		Random r = new Random();
		Scanner s = new Scanner(System.in);
		int playerGuess;
		boolean keepPlaying = true;
		
		// I don't know why this is here
		while(keepPlaying) { 
			System.out.println("Please guess the year (or Q to quit)");
			Date randomDate = getRandomDate(); 
			int year 	= randomDate.getYear(); 
			int month 	= randomDate.getMonth();
			int day 	= randomDate.getDay();
			
			while(s.hasNext()) {
				if(s.hasNextInt()) {
					playerGuess = s.nextInt();
					if(playerGuess < year) {
						System.out.println("Wrong. Higher. Please guess the year: ");
					}else if(playerGuess > year) {
						System.out.println("Wrong. Lower. Please guess the year: ");
					}else if(playerGuess == year){
						System.out.println("Correct. Please guess the month: ");
						while(keepPlaying) {
							Scanner s2 = new Scanner(System.in);
							playerGuess = s2.nextInt();
							// checks if you entered a number out of bounds
							if(playerGuess < Date.JANUARY || playerGuess > Date.NUMBER_OF_MONTHS) { 
								System.out.println("Wrong. " + playerGuess + " is not a valid month: ");
							}else if(playerGuess < month) {
								System.out.println("Wrong. Higher. Please guess the month: ");
							}else if(playerGuess > month) {
								System.out.println("Wrong. Lower. Please guess the month: ");
							}else if(playerGuess == month) {
								System.out.println("Correct. Please guess the day:");
								while(keepPlaying) {
									Scanner s3 = new Scanner(System.in);
									playerGuess = s3.nextInt();
									// checks February and leap year
									if(month == Date.FEBRUARY && playerGuess > Date.FEBRUARY_LEAP_YEAR && randomDate.isLeapYear() == true) { 
										System.out.println("Wrong. " + playerGuess + " is not a valid day for month: " + month + ". Please guess the day: "); 
									// checks February not leap year
									}else if(month == Date.FEBRUARY && playerGuess > Date.FEBRUARY_MAX_DAY && randomDate.isLeapYear() == false) { 
										System.out.println("Wrong. " + playerGuess + " is not a valid day for month: " + month + ". Please guess the day: "); 
									// checks months with 30 days
									}else if(month % 2 == 0 && playerGuess > Date.EVEN_MONTH_MAX_DAY) { 
										System.out.println("Wrong. " + playerGuess + " is not a valid day for month: " + month + ". Please guess the day: "); 
									// checks months with 31 days
									}else if(month % 2 != 0 && playerGuess > Date.ODD_MONTH_MAX_DAY) { 
										System.out.println("Wrong. " + playerGuess + " is not a valid day for month: " + month + ". Please guess the day: "); 
									}else if(playerGuess < day) {
										System.out.println("Wrong. Higher. Please guess the day: ");
									}else if(playerGuess > day) {
										System.out.println("Wrong. Lower. Please guess the day:");
									}else if(playerGuess == day) {
										System.out.println("Correct. "+ randomDate.displayDate());
										keepPlaying = false;
										guessTheDate();
										
									}
								}
							}
						}
					}
				}else if(s.hasNext()) {
					String input = s.next();
					if(input.equalsIgnoreCase("q")) {
						System.out.println("The game is over. Thank you.");
						return;
					}
				}
			}
		}
	}
	
	/**
	 * Guess the birthday game.
	 */
	public static void guessTheBirthDay() {
		Scanner s = new Scanner(System.in);
		Random r = new Random();
		
		int score = Date.ZERO; // score count
		int index = Date.ONE; // index for game number
		
		System.out.println("Welcome to guess the birthday! Press Q to quit.");
		while(index <= MAX_GAMES) {
			//creates a new date between 1900 and 2018
			Date randomDate = new Date(r.nextInt(BIRTHDAY_MAX_YEAR - BIRTHDAY_MIN_YEAR) + BIRTHDAY_MIN_YEAR, r.nextInt(Date.MAX_MONTH) + Date.ONE, r.nextInt(Date.MAX_DAY) + Date.ONE); 
			System.out.println("Date #" + index + ": What day of the week was " + randomDate.display() + ":");
			while(s.hasNext()) {
				if(s.hasNext()) {	
					String input = s.next();
					if(input.equalsIgnoreCase(randomDate.getDayOfTheWeek())) {
						System.out.println("Correct!");
						index++;
						score++;
						break;
					}else if(input.equalsIgnoreCase("q")){
						System.out.println("Goodbye!");
						return;
					}else {
						System.out.println("Wrong!");
						index++;
						break;
					}
				}
			}
		}
		System.out.println("You scored " + score + " out of 5.");
	}
}

				
				
					