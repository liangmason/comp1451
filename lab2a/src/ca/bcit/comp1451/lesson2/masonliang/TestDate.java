package ca.bcit.comp1451.lesson2.masonliang;

public class TestDate {

	public static void main(String[] args) {
		Date d = new Date (2018, 6, 20); //Wednesday not leap year
		Date d2 = new Date (2016, 12, 25); //Sunday leap year
		
		
		System.out.println(d.getDayOfTheWeek());
		System.out.println(d.isLeapYear());
		System.out.println(d.displayDate());
		System.out.println(d2.getDayOfTheWeek());
		System.out.println(d2.isLeapYear());


	}

}
