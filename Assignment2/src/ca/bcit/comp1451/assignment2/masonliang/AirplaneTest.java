/**
 * 
 */
package ca.bcit.comp1451.assignment2.masonliang;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.Test;

/**
 * Car test
 * @author Mason
 *
 */
public class AirplaneTest {
	private Airplane v = new Airplane(1998, "ABC Motors", "Comac", 10000);
	private Airplane v2 = new Airplane(1940, "Boeing", "84", 45000);
	
	@Test
	public void testConstructor() {
		new Airplane(1998, "ABC Motors", "Comac", 10000);
		
	}
	
	@Test
	public void testConstructor2() {
		new Airplane(2004, "Boeing", "777", 123456);
	}
	
	@Test
	public void testGetYearManufacturered() {
		assertEquals(1998, v.getYearManufactured());
	}
	
	@Test
	public void testGetYearManufacturered2() {
		assertEquals(1940, v2.getYearManufactured());
	}
	
	@Test
	public void testBadSetYearManufactureredOver() {
		try {
			new Airplane(0, "ABC Motors", "Comac", 10000);
			fail("year over 2018 or less than 1903 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetYearManufactureredLessThan() {
		try {
			new Airplane(2019, "ABC Motors", "Comac", 10000);
			fail("year over 2018 or less than 1903 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testGetMake() {
		assertEquals("ABC Motors", v.getMake());
	}
	
	@Test
	public void testGetMake2() {
		assertEquals("Boeing", v2.getMake());
	}
	
	@Test
	public void testBadSetMakeNull() {
		try {
			new Airplane(2018, null, "777", 10000);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetMakeEmpty() {
		try {
			new Airplane(1998, "", "Comac", 10000);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testGetModel() {
		assertEquals("ABC Motors", v.getMake());
	}
	
	@Test
	public void testGetModel2() {
		assertEquals("Boeing", v2.getMake());
	}
	
	@Test
	public void testBadSetModelNull() {
		try {
			new Airplane(2018, "Boeing", null, 10000);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetModelEmpty() {
		try {
			new Airplane(1998, "ABC Motors", "", 10000);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testGetMaximumHeightFeet() {
		assertEquals(10000, v.getMaximumHeightFeet());
	}
	
	@Test
	public void testGetMaximumHeightFeet2() {
		assertEquals(45000, v2.getMaximumHeightFeet());
	}
	
	@Test
	public void testBadSetMaximumHeightFeetOver() {
		try {
			new Airplane(1998, "ABC Motors", "Comac", 500000);
			fail("feet cannot be less than 1000 or over 125000 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid max feet",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetMaximumHeightFeetLessThan() {
		try {
			new Airplane(2018, "ABC Motors", "Comac", 0);
			fail("feet cannot be less than 1000 or over 125000 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid max feet",  e.getMessage());
		}
	}
	
	@Test
	public void testToString() {
		assertEquals("This airplane is a 1998 ABC Motors Comac that can reach 10000 feet.", v.toString());
	}
	
	@Test
	public void testToString2() {
		assertEquals("This airplane is a 1940 Boeing 84 that can reach 45000 feet.", v2.toString());
	}
	
	@Test
	public void testEqualsTrue() {
		Airplane v = new Airplane(1998, "ABC Motors", "Comac", 10000);
		Airplane v2 = new Airplane(1998, "ABC Motors", "Comac", 15000);
		
		assertTrue(v.equals(v2) && v2.equals(v));
	}
	
	@Test
	public void testEqualsFalse() {
		assertFalse(v.equals(v2) && v2.equals(v));
		assertFalse(v.hashCode() == v2.hashCode());
	}
	
	@Test
	public void testCompareTo() {
		assertTrue(v.compareTo(v2) < 0);
	}
	
	@Test
	public void testCompareToMaximumHeightFeet() {
		Airplane v = new Airplane(1998, "ABC Motors", "Comac", 10000);
		Airplane v2 = new Airplane(1998, "ABC Motors", "Comac", 10000);
		
		assertTrue(v.compareTo(v2) == 0);
	}
}
