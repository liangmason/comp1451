/**
 * 
 */
package ca.bcit.comp1451.assignment2.masonliang;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Mason
 *
 */
public class VechicleTest extends Vehicle{
	private Vehicle v;
	private Vehicle v2;

	
	@Before
	public void setUp() {
		v = new Vehicle(1998, "Something", "Stuff") {};
		v2 = new Vehicle(2018, "A", "Vehicle") {};
	}
	
	@Test
	public void testGetYearManufactured() {
		assertEquals(1998, v.getYearManufactured());
	}
	
	@Test
	public void testGetYearManufactured2() {
		assertEquals(2018, v2.getYearManufactured());
	}
	
	@Test
	public void testBadYearManufacturered() {
		try {
			v = new Vehicle(0, "Something", "Stuff") {};
			fail("year must be within 0 and 9999");
		}catch(Exception e){
			assertEquals("invalid year", e.getMessage());
		}
	}
	
	@Test
	public void testBadYearManufacturered2() {
		try {
			v = new Vehicle(10000, "Something", "Stuff") {};
			fail("year must be within 0 and 9999");
		}catch(Exception e){
			assertEquals("invalid year", e.getMessage());
		}
	}
	
	@Test
	public void testGetMake() {
		assertEquals("Something", v.getMake());
	}
	
	@Test
	public void testGetMake2() {
		assertEquals("A", v2.getMake());
	}
	
	@Test
	public void testBadSetMakeNull() {
		try {
			v = new Vehicle(1998, null, "Stuff") {};
			fail("make is null");
		}catch(Exception e){
			assertEquals("invalid make", e.getMessage());
		}
	}
	
	@Test
	public void testBadSetMakeEmpty() {
		try {
			v = new Vehicle(2018, "", "Stuff") {};
			fail("make is empty");
		}catch(Exception e){
			assertEquals("invalid make", e.getMessage());
		}
	}
	
	@Test
	public void testToString() {
		assertEquals("This vehicle is a 1998 Something Stuff.", v.toString());
	}
	
	@Test
	public void testToString2() {
		assertEquals("This vehicle is a 2018 A Vehicle.", v2.toString());
	}
	
	@Test
	public void testEqualsTrue() {
		v  = new Vehicle(1998, "Something", "Stuff") {};
		v2 = new Vehicle(1998, "Something", "Stuff") {};
		
		assertTrue(v.equals(v2) && v2.equals(v));
	}
	
	@Test
	public void testEqualsFalse() {
		assertFalse(v.equals(v2) && v2.equals(v));
		assertFalse(v.hashCode() == v2.hashCode());
	}
	
}
