package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */


/**
 * Steerable interface
 * @author liang
 *
 */
public interface Steerable {
	void accelerate();
	void steerLeft();
	void steerRight();
}
