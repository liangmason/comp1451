/**
 * 
 */
package ca.bcit.comp1451.assignment2.masonliang;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.Test;

/**
 * Book test
 * @author Mason
 *
 */
public class BoatTest {
	private Boat v = new Boat(1998, "Bayliner", "Extreme", true);
	private Boat v2 = new Boat(1940, "Boesch", "Journey", false);
	
	@Test
	public void testConstructor() {
		new Boat(1980, "Bayliner", "Extreme", true);
		
	}
	
	@Test
	public void testConstructor2() {
		new Boat(2000, "Boesch", "Journey", false);
	}
	
	@Test
	public void testGetYearManufacturered() {
		assertEquals(1998, v.getYearManufactured());
	}
	
	@Test
	public void testGetYearManufacturered2() {
		assertEquals(1940, v2.getYearManufactured());
	}
	
	@Test
	public void testBadSetYearManufactureredOver() {
		try {
			new Boat(2345, "Bayliner", "Extreme", true);
			fail("year over 2018 or less than 0 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetYearManufactureredLessThan() {
		try {
			new Boat(0, "Boesch", "Journey", false);
			fail("year over 2018 or less than 0 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testGetMake() {
		assertEquals("Bayliner", v.getMake());
	}
	
	@Test
	public void testGetMake2() {
		assertEquals("Boesch", v2.getMake());
	}
	
	@Test
	public void testBadSetMakeNull() {
		try {
			new Boat(2000, null, "Extreme", true);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetMakeEmpty() {
		try {
			new Boat(2013, "", "Journey", false);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testGetModel() {
		assertEquals("Bayliner", v.getMake());
	}
	
	@Test
	public void testGetModel2() {
		assertEquals("Boesch", v2.getMake());
	}
	
	@Test
	public void testBadSetModelNull() {
		try {
			new Boat(1998, "Bayliner", null, true);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetModelEmpty() {
		try {
			new Boat(1940, "Boesch", "", false);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testMotorized() {
		assertEquals(true, v.getMotorized());
	}
	
	@Test
	public void testMotorized2() {
		assertEquals(false, v2.getMotorized());
	}
	
	
	@Test
	public void testToString() {
		assertEquals("This boat is a 1998 Bayliner Extreme (with motor).", v.toString());
	}
	
	@Test
	public void testToString2() {
		assertEquals("This boat is a 1940 Boesch Journey (without motor).", v2.toString());
	}
	
	@Test
	public void testEqualsTrue() {
		Boat v  = new Boat(1998, "Bayliner", "Extreme", true);
		Boat v2  = new Boat(1998, "Bayliner", "Extreme", true);
		
		assertTrue(v.equals(v2) && v2.equals(v));
	}
	
	@Test
	public void testEqualsFalse() {
		assertFalse(v.equals(v2) && v2.equals(v));
		assertFalse(v.hashCode() == v2.hashCode());
	}
	
	@Test
	public void testCompareTo() {
		Boat v  = new Boat(1998, "Bayliner", "Extreme", true);
		Boat v2  = new Boat(1998, "Bayliner", "Extreme", true);
		
		assertTrue(v.compareTo(v2) == 0);
	}
	
	@Test
	public void testCompareToNewer() {
		Boat v  = new Boat(1998, "Bayliner", "Extreme", true);
		Boat v2 = new Boat(1940, "Boesch", "Journey", true);;
		
		assertTrue(v.compareTo(v2) > 0);
	}
}
