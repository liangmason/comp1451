package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */


/**
 * This class defines a airplane
 * @author Mason
 *
 */
public class Airplane extends Vehicle implements Comparable<Airplane>, Steerable{
	private int maximumHeightFeet;
	
	private static final int MIN_YEAR = 1903;
	private static final int MAX_YEAR = 2018;

	private static final int MIN_HEIGHT_FEET = 1000;
	private static final int MAX_HEIGHT_FEET = 125000;
	
	private static final int HEIGHT_FEET_RANGE = 10000;
	
	/**
	 * @param yearManufactured Year make of plane
	 * @param make Make of plane
	 * @param model Model of plane
	 * @param maximumHeightFeet Max height in feet plane can fly
	 */
	public Airplane(int yearManufactured, String make, String model, int maximumHeightFeet) {
		super(yearManufactured, make, model);
		setMaximumHeightFeet(maximumHeightFeet);
	}

	/**
	 * @return the maximumHeightFeet
	 */
	public int getMaximumHeightFeet() {
		return maximumHeightFeet;
	}

	/**
	 * @param maximumHeightFeet the maximumHeightFeet to set
	 */
	public void setMaximumHeightFeet(int maximumHeightFeet) {
		if(maximumHeightFeet <= MIN_HEIGHT_FEET || maximumHeightFeet >= MAX_HEIGHT_FEET) {
			throw new IllegalArgumentException("invalid max feet");
		}
		this.maximumHeightFeet = maximumHeightFeet;
	}
	
	@Override
	/**
	 * @param yearManufactured the yearManufactured to set
	 */
	public void setYearManufactured(int yearManufactured) {
		if(yearManufactured <= MIN_YEAR || yearManufactured > MAX_YEAR) {
			throw new IllegalArgumentException("invalid year");
		}
		super.setYearManufactured(yearManufactured);;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "This " + getClass().getSimpleName().substring(0).toLowerCase() + " is a " + getYearManufactured() + " " + getMake() + " " + getModel() + " that can reach " + getMaximumHeightFeet() + " feet.";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + maximumHeightFeet;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Airplane))
			return false;
		if (obj instanceof Vehicle)
			return true;
		Airplane other = (Airplane) obj;
		if (this.getMaximumHeightFeet() != other.getMaximumHeightFeet())
			return false;
		if (Math.abs(this.getMaximumHeightFeet() - other.getMaximumHeightFeet()) <= HEIGHT_FEET_RANGE) {
			return true;
		}
		return true;
	}
	
	/**
	 * Airplanes that can fly higher are better
	 */
	@Override
	public int compareTo(Airplane other) {
		return Integer.compare(this.getMaximumHeightFeet(), other.getMaximumHeightFeet());
	}
	
	@Override
	public void accelerate() {
		System.out.println("fire engines on wings");
	}
	
	@Override
	public void steerRight() {
		System.out.println("lift wing flaps to turn right");
	}
	
	@Override
	public void steerLeft() {
		System.out.println("lift wing flaps to turn left");
	}
}
