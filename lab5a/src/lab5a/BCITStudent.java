package lab5a;

public class BCITStudent extends Student{
	private String campus;
	private String BCITStudentNumber;

	/**
	 * @param firstName
	 * @param lastName
	 * @param yearOfBirth
	 * @param gender
	 * @param studentNumber
	 * @param major
	 * @param campus
	 */
	public BCITStudent(String firstName, String lastName, int yearOfBirth, String gender, int studentNumber,
			String major, String campus) {
		super(firstName, lastName, yearOfBirth, gender, studentNumber, major);
		setCampus (campus);
		
		this.BCITStudentNumber = String.format("A" + String.valueOf(getStudentNumber()));
	}

	/**
	 * @return the campus
	 */
	public String getCampus() {
		return campus;
	}
	

	/**
	 * @return the bCITStudentNumber
	 */
	public String getBCITStudentNumber() {
		return BCITStudentNumber;
	}

	/**
	 * @param campus the campus to set
	 */
	public void setCampus(String campus) {
		switch(campus) {
			case ("Burnaby"):
			case ("Downtown"):
			case ("Richmond"):
			case ("North Vancouver"):
				this.campus = campus;
				break;
			default:
				throw new IllegalArgumentException("invalid campus");
		}
	}
	
	
	public void printDetails() {
		if(getGender().equalsIgnoreCase("male")) {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + " of " + getMajor() + " (st# " + getBCITStudentNumber() + "). at the " + getCampus() + " campus. He was born in " + getYearOfBirth() + ".");
		}else {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + " of " + getMajor() + " (st# " + getBCITStudentNumber() + "). at the " + getCampus() + " campus. She was born in " + getYearOfBirth() + ".");
		}
	}
}
