import java.util.HashMap;
import java.util.Map;

/**
 * 
 */

/**
 * Material enum for shoe
 * @author Mason
 *
 */
public enum Material {
	PLASTIC("plastic"), LEATHER("leather"), RUBBER("rubber"), CLOTH("cloth");
	
	private String theMaterial;
	
	private static Map<String, Material> lookup = new HashMap<String, Material>();
	
	/**
	 * ShoeType constructor
	 * @param Material
	 */
	private Material(String Material) {
		this.theMaterial = Material;
	}
	
	/**
	 * get method for shoe type
	 * 
	 * @return theShoeType
	 */
	public String getTheMaterial() {
		return theMaterial;
	}
	
	/**
	 * Creates a Material enum from a string
	 * 
	 * @param theMaterial Shoe Type string
	 * @return Material enum
	 */
	public static Material get(String theMaterial) {
		return lookup.get(theMaterial);
	}

	// Prints out all BookType enums and BookType string
	static {
		for (Material m : Material.values()) {
			lookup.put(m.getTheMaterial(), m);
		}
	}
}
