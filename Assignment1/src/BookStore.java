import java.util.Collection;
import java.util.Iterator;


/**
 * 
 */

/**
 * This class defines a book store
 * @author Mason
 *
 */
public class BookStore extends Store{
	public BookType specialty;
	
	/**
	 * @param address of store
	 * @param name of store 
	 * @param specialty of store
	 */
	public BookStore(Address address, String name, BookType specialty) {
		super(address, name);
		this.specialty = specialty;
		addBooks();
	}
	
	/**
	 * @param address of store
	 * @param name of store 
	 * @param specialty of store
	 */
	public BookStore(Address address, String name, String specialty) {
		super(address, name);
		this.specialty = BookType.get(specialty);
		addBooks();
	}

	/**
	 * add books to item list
	 */
	public void addBooks() {
		Date birthDate = new Date(1919, 1, 1);
		Name name = new Name("Jerome", "David", "Salinger");
		BookType genre = BookType.get("fiction");
		Author author = new Author(birthDate, name, genre, "JD");
		Date datePublished = new Date(1951, 5, 14);
		String title = "The Catcher in the Rye";
		Book b = new Book(0.4, 2.0, 4.0, "1234", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(1948, 1, 31);
		title = "A Perfect Day for Bananafish";
		genre = BookType.get("fiction");
		b = new Book(1, 11, 12, "2345", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(1945, 12, 12);
		title = "A Boy in France";
		genre = BookType.get("fiction");
		b = new Book(2, 33, 35, "3456", author, datePublished, title, genre);
		addItem(b);

		birthDate = new Date(1963, 9, 3);
		name = new Name("Malcolm", "Gladwell");
		genre = BookType.get("nonfiction");
		author = new Author(birthDate, name, genre);
		datePublished = new Date(2008, 11, 18);
		title = "Outliers";
		b = new Book(2.1, 2, 6, "4567", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(2000, 3, 1);
		title = "The Tipping Point";
		genre = BookType.get("nonfiction");
		b = new Book(0.5, 3, 5, "5678", author, datePublished, title, genre);
		addItem(b);

		birthDate = new Date(1919, 11, 26);
		name = new Name("Frederik", "Pohl");
		genre = BookType.get("sciencefiction");
		author = new Author(birthDate, name, genre, "Paul Dennis Lavond");
		datePublished = new Date(1977, 7, 4);
		title = "Gateway";
		b = new Book(0.01, 4, 4, "6789", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(1937, 10, 6);
		title = "Elegy to a Dead Planet: Luna";
		genre = BookType.get("sciencefiction");
		b = new Book(0.1, 5, 11, "abcd", author, datePublished, title, genre);
		addItem(b);

		birthDate = new Date(1918, 5, 11);
		name = new Name("Richard", "Phillips", "Feynman");
		genre = BookType.get("reference");
		author = new Author(birthDate, name, genre);
		datePublished = new Date(1942, 5, 20);
		title = "Principle of Least Action in Quantum Mechanics";
		b = new Book(0.8, 15, 30, "efgh", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(1964, 6, 30);
		title = "The Messenger Lectures";
		genre = BookType.get("reference");
		b = new Book(0.6, 44, 45.5, "ijkl", author, datePublished, title, genre);
		addItem(b);

		datePublished = new Date(1985, 11, 1);
		title = "Surely You're Joking Mr. Feynman";
		genre = BookType.get("nonfiction");
		b = new Book(1.0, 3, 13, "mnop", author, datePublished, title, genre);
		addItem(b);
	}
	
	/**
	 * Gives you all books by author older than ageInYears
	 * @param ageInYears age of author
	 */
	public void displayAllBooksWrittenByAuthorsOverThisAge(int ageInYears) {
		Collection<Book> books = getCollectionOfItems(); // From the Store class
		Iterator<Book> it = books.iterator();
		boolean displayedSome = false;
		while (it.hasNext()) {
			Book b = it.next();
			int ageYears = b.getDatePublished().getYear() - b.getAuthor().getBirthDate().getYear();
			if (ageYears > ageInYears) {
				System.out.println(b.getTitle() + " was written by " + b.getAuthor().getName().getLastName()
						+ " at age " + ageYears + ", which is more than " + ageInYears);
				displayedSome = true;
			}
		}
		if (displayedSome == false) {
			System.out.println("No books by authors over age " + ageInYears);
		}
	}
	
	/**
	 * Gives you all books by all authors
	 */
	public void displayAllBooksByEveryAuthor() {
		Collection<Book> books = getCollectionOfItems();
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()){
			Book b = it.next();
			System.out.println(b.getAuthor().getName().getLastName() + " wrote " + b.getTitle() + " in " + b.getDatePublished().getYear());
		}
	}
	
	/**
	 * Gives your all books written my author last name
	 * @param lastName last name of author
	 */
	public void displayAllBooksByAuthor(String lastName) {
		Collection<Book> books = getCollectionOfItems();
		boolean displayedSome = false;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getAuthor().getName().getLastName().equals(lastName) && lastName != null) {
				System.out.println(b.getAuthor().getName().getLastName() + " wrote " + b.getTitle());
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("There are no authors with the last name: " + lastName);
		}
	}
	
	/**
	 * Gives you all books written before a year
	 * @param year before 
	 */
	public void displayAllBooksWrittenBefore(int year) {
		Collection<Book> books = getCollectionOfItems();
		boolean displayedSome = false;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getDatePublished().getYear() < year) {
				System.out.println(b.getTitle() + " was published in " + b.getDatePublished().getYear() + ", which is before " + year);
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("No books published before " + year);
		}
	}
	
	/**
	 * Gives books with author's pseudonym
	 * @param pseudonym
	 */
	public void displayTitlesOfBooksWrittenBy(String pseudonym) {
		Collection<Book> books = getCollectionOfItems();
		Iterator<Book> it = books.iterator();
		
		if(pseudonym == null) {
			System.out.println("pseudonym is null");
			return;
		}
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getAuthor().getPseudonym() == (pseudonym) && pseudonym != null) {
				System.out.println(b.getAuthor().getName().getLastName() + " wrote " + b.getTitle() + " as " + b.getAuthor().getPseudonym());
			}
		}
		
	}
	
	/**
	 * Display all books with inputed genre
	 * @param genre
	 */
	public void displayAllBooksForGenre(String genre) {
		Collection<Book> books = getCollectionOfItems();
		boolean displayedSome = false;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getGenreString().equals(genre) && genre != null) {
				System.out.println(b.getTitle() + " is a " + b.getGenreString() + " book written by " + b.getAuthor().getName().getLastName());
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("No books with a genre of " + genre);
		}
	}
	
	/**
	 * Display sum of weight of all books
	 */
	public void displayTotalWeightKgOfAllBooks() {
		Collection<Book> books = getCollectionOfItems();
		double sumOfBooksWeightKg = 0;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();	
			sumOfBooksWeightKg += b.getWeightKg();
		}
		System.out.println("total kg of books: " + sumOfBooksWeightKg);
	}
	
	/**
	 * Gets you all authors and their books that was born on a specific day of the week
	 * @param dayOfTheWeek
	 */
	public void displayAllBooksWrittenByAuthorsBornOn(String dayOfTheWeek) {
		Collection<Book> books = getCollectionOfItems();
		boolean displayedSome = false;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getAuthor().getBirthDate().getDayOfTheWeek().equalsIgnoreCase(dayOfTheWeek) && dayOfTheWeek != null) {
				System.out.println(b.getTitle() + " was written by " + b.getAuthor().getName().getLastName() + ", who was born on a " + b.getAuthor().getBirthDate().getDayOfTheWeek());
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("No authors were born on a " + dayOfTheWeek);
		}
	}
	
	/**
	 * Gives you all books published on a day of a week
	 * @param dayOfTheWeek
	 */
	public void displayAllBooksPublishedOn(String dayOfTheWeek) {
		Collection<Book> books = getCollectionOfItems();
		boolean displayedSome = false;
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getDatePublished().getDayOfTheWeek().equalsIgnoreCase(dayOfTheWeek) && dayOfTheWeek != null) {
				System.out.println(b.getTitle() + " was written by " + b.getAuthor().getName().getLastName() + ", which was published on a " + b.getDatePublished().getDayOfTheWeek());
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("No books were published on a Sunday");
		}
	}
	
	/**
	 * Gives you all authors with a pseudonym
	 */
	public void displayAllBooksWrittenByAuthorsWithAPseudonym() {
		Collection<Book> books = getCollectionOfItems();
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b.getAuthor().getPseudonym() != null) {
				System.out.println(b.getAuthor().getName().getLastName() + " wrote " + b.getTitle() + " as " + b.getAuthor().getPseudonym());
			}
		}
	}
	
	/**
	 * Gives you the book with the biggest markup
	 */
	public void displayBookWithBiggestPercentageMarkup() {
		Collection<Book> books = getCollectionOfItems();
		Iterator<Book> it = books.iterator();
		double highestMarkUpPercent = 0;
		Book highestPercentBook = null;
		
		while(it.hasNext()) {
			Book b = it.next();
			double percentMarkUp = b.getMarkUpPercent();
			
			if(percentMarkUp > highestMarkUpPercent) {
				highestMarkUpPercent = percentMarkUp;
				highestPercentBook = b;
			}
		}
		System.out.println("Highest markup is " + highestMarkUpPercent + "%, for " + highestPercentBook.getTitle()  + " by " + highestPercentBook.getAuthor().getName().getLastName());
	}
	
	/**
	 * Gives you all books written outside author's specialty
	 */
	public void displayAllBooksWrittenOutsideSpecialty() {
		Collection<Book> books = getCollectionOfItems();
		Iterator<Book> it = books.iterator();
		
		while(it.hasNext()) {
			Book b = it.next();
			if(!b.getAuthor().getGenre().equals(b.getGenre())) {
				System.out.println(b.getAuthor().getName().getLastName() + " usually wrote " + b.getAuthor().getGenre().getTheBookType() + " but wrote " + b.getTitle() + " which is " + b.getGenreString());
			}
		}
	}
}
