/**
 * 
 */

/**
 * This is a name class.
 * @author Mason
 *
 */
public class Name {
	private String firstName;
	private String middleName;
	private String lastName;
	
	/**
	 * For name with a middle name
	 * @param firstName first name
	 * @param middleName midde name
	 * @param lastName last name
	 */
	public Name(String firstName, String middleName, String lastName) {
		setFirstName(firstName);
		setMiddleName(middleName);
		setLastName(lastName);
	}
	
	/**
	 * For name without a middle name
	 * @param firstName first name
	 * @param lastName last name
	 */
	public Name(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}
	
	/**
	 * For name with only one name, used in shoeStore
	 * @param firstName first name
	 */
	public Name(String firstName) {
		setFirstName(firstName);
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		if(firstName != null) {
			this.firstName = firstName;
		}else {
			System.out.println("first name is null");
		}
	}

	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		if(middleName != null) {
			this.middleName = middleName;
		}else {
			System.out.println("middle name is null");
		}
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		if(lastName != null) {
			this.lastName = lastName;
		}else {
			System.out.println("last name is null");
		}
	}
	
	/**
	 * Gives you full name, also if middle name is empty or null, don't give it
	 * @return full name
	 */
	public String fullName() {
		String fullName = "";
        if  (firstName != null) {
            fullName = fullName + firstName + " ";
        }
        if  (middleName != null) {
            fullName = fullName + middleName + " ";
        }
        if  (lastName != null) {
            fullName = fullName + lastName;
        }
        return fullName;
	}
}
