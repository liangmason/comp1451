import java.util.HashMap;
import java.util.Map;

/**
 * Enum of book types
 * 
 * @author Mason
 *
 */

public enum BookType {
	FICTION("fiction"), NONFICTION("nonfiction"), SCIENCEFICTION("sciencefiction"), REFERENCE("reference");

	private String theBookType;

	private static Map<String, BookType> lookup = new HashMap<String, BookType>();

	/**
	 * BookType constructor
	 * 
	 * @param theBookType
	 *            BookType
	 */
	private BookType(String theBookType) {
		this.theBookType = theBookType;
	}

	/**
	 * get method for theBookType
	 * 
	 * @return theBookType
	 */
	public String getTheBookType() {
		return theBookType;
	}

	/**
	 * Creates a BookType enum from a string
	 * 
	 * @param theBookType Book Type string
	 * @return BookType enum
	 */
	public static BookType get(String theBookType) {
		return lookup.get(theBookType);
	}

	// Prints out all BookType enums and BookType string
	static {
		for (BookType b : BookType.values()) {
			lookup.put(b.getTheBookType(), b);
		}
	}
}
