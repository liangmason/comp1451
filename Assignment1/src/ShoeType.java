import java.util.HashMap;
import java.util.Map;

/**
 * 
 */

/**
 * Shoe type enum
 * @author Mason
 *
 */
public enum ShoeType {
	WOMEN("women"), MEN("men"), CHILDREN("children"), SPORTS("sports"), DRESS("dress");
	
	private String theShoeType;
	
	private static Map<String, ShoeType> lookup = new HashMap<String, ShoeType>();
	
	/**
	 * ShoeType constructor
	 * @param ShoeType
	 */
	private ShoeType(String ShoeType) {
		this.theShoeType = ShoeType;
	}
	
	/**
	 * get method for shoe type
	 * 
	 * @return theShoeType
	 */
	public String getTheShoeType() {
		return theShoeType;
	}
	
	/**
	 * Creates a ShoeType enum from a string
	 * 
	 * @param theShoeType Shoe Type string
	 * @return ShoeType enum
	 */
	public static ShoeType get(String theShoeType) {
		return lookup.get(theShoeType);
	}

	// Prints out all BookType enums and BookType string
	static {
		for (ShoeType s : ShoeType.values()) {
			lookup.put(s.getTheShoeType(), s);
		}
	}
}
