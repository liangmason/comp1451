import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 
 */

/**
 * This class defines a item
 * @author Mason Liang
 * @version 1.0
 */
public class Item {
	private double weightKg;
	private double manufacturingPriceDollars;
	private double suggestedRetailPriceDollars;
	private String uniqueID;
	public static final int markUpToPercent = 100;
	
	
	/**
	 * @param weightKg weight of item in kg
	 * @param manufacturingPriceDollars mrsp
	 * @param suggestedRetailPriceDollars retail price
	 * @param uniqueID of item
	 */
	public Item(double weightKg, double manufacturingPriceDollars, double suggestedRetailPriceDollars,
			String uniqueID) {
		setWeightKg(weightKg);
		setManufacturingPriceDollars(manufacturingPriceDollars);
		setSuggestedRetailPriceDollars(suggestedRetailPriceDollars);
		setUniqueID(uniqueID);
	}
	
	/**
	 * @return the weightKg
	 */
	public double getWeightKg() {
		return weightKg;
	}
	
	/**
	 * @return the manufacturingPriceDollars
	 */
	public double getManufacturingPriceDollars() {
		return manufacturingPriceDollars;
	}
	
	/**
	 * @return the suggestedRetailPriceDollars
	 */
	public double getSuggestedRetailPriceDollars() {
		return suggestedRetailPriceDollars;
	}
	
	/**
	 * @return the mark up percent
	 */
	public double getMarkUpPercent() {
		double price =  getSuggestedRetailPriceDollars();
		double MRSP = getManufacturingPriceDollars();
		double profit = price - MRSP;
		double markUp = (profit / MRSP) * markUpToPercent;
		return markUp;
	}
	
	/**
	 * @return the uniqueID
	 */
	public String getUniqueID() {
		return uniqueID;
	}
	
	/**
	 * @param weightKg the weightKg to set
	 */
	public void setWeightKg(double weightKg) {
		this.weightKg = weightKg;
	}
	
	/**
	 * @param manufacturingPriceDollars the manufacturingPriceDollars to set
	 */
	public void setManufacturingPriceDollars(double manufacturingPriceDollars) {
		if(manufacturingPriceDollars > 0) {
			this.manufacturingPriceDollars = manufacturingPriceDollars;
		}else {
			System.out.println("not a valid price");
		}
	}
	
	/**
	 * @param suggestedRetailPriceDollars the suggestedRetailPriceDollars to set
	 */
	public void setSuggestedRetailPriceDollars(double suggestedRetailPriceDollars) {
		if(suggestedRetailPriceDollars > 0) {
			this.suggestedRetailPriceDollars = suggestedRetailPriceDollars;
		}else {
			System.out.println("not a valid price");
		}
	}
	
	/**
	 * @param uniqueID the uniqueID to set
	 */
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	
	/**
	 * Rounds double value to decimal place
	 * @param value you want to round 
	 * @param places the decimal place
	 * @return
	 */
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
