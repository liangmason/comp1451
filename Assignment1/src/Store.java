import java.util.Collection;
import java.util.HashMap;

/**
 * This class defines a store
 * @author liang
 *
 */
public class Store {
	private Address streetAddress;
	private String name;
	
	private HashMap<String, Item> itemsForSale = new HashMap<String, Item>();

	/**
	 * @param streetAddress of store
	 * @param name of store
	 */
	public Store(Address streetAddress, String name) {
		setStreetAddress(streetAddress);
		setName(name);
	}
	
	/**
	 * @return the streetAddress
	 */
	public Address getStreetAddress() {
		return streetAddress;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(Address streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * add a item to the itemsforSale hashmap
	 * @param item
	 */
	public void addItem(Item item){
        itemsForSale.put(item.getUniqueID(), item);
    }
	
	/**
	 * lookup a item that is for sale by key
	 * @param key
	 * @return
	 */
	public Item getItemByKey(String key){
        return itemsForSale.get(key);
    }
	
	/**
	 * 
	 * @return get values of 
	 */
	public Collection getCollectionOfItems(){
		return itemsForSale.values();
	}

}
