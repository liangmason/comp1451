import java.awt.Color;
import java.util.Collection;

/**
 * 
 */

/**
 * This class defines a shoe
 * @author liang
 *
 */
public class Shoe extends Item{
	private Material material;
	private int size;
	private ShoeType shoeType;
	private Color color;
	private Name designer;
	
	
	/**
	 * @param weightKg weight of shoe in kg
	 * @param manufacturingPriceDollars mrsp of shoe
	 * @param suggestedRetailPriceDollars retail price of shoe
	 * @param uniqueID of shoe
	 * @param material of shoe
	 * @param size of shoe
	 * @param shoeType of shoe
	 * @param color of shoe
	 */
	public Shoe(double weightKg, double manufacturingPriceDollars, double suggestedRetailPriceDollars, String uniqueID,
			Name designer, int size, Material material, Color color, ShoeType shoeType) {
		super(weightKg, manufacturingPriceDollars, suggestedRetailPriceDollars, uniqueID);
		setMaterial(material);
		setSize(size);
		setShoeType(shoeType); 
		setColor(color);
		setDesigner(designer);
	}
	
	
	/**
	 * @return the material
	 */
	public Material getMaterial() {
		return material;
	}


	/**
	 * @param material the material to set
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}


	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}


	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		if(size <= 25) {
			this.size = size;
		}else {
			System.out.println("size too big, must be less than 20");
		}
	}


	/**
	 * @return the shoeType
	 */
	public ShoeType getShoeType() {
		return shoeType;
	}


	/**
	 * @param shoeType the shoeType to set
	 */
	public void setShoeType(ShoeType shoeType) {
		this.shoeType = shoeType;
	}


	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}


	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}


	/**
	 * @return the designer
	 */
	public Name getDesigner() {
		return designer;
	}


	/**
	 * @param designer the designer to set
	 */
	public void setDesigner(Name designer) {
		this.designer = designer;
	}


	/**
	 * Calls get method for uniqueID in Item class
	 * @return Description
	 */
	public String getDescription() {
		return getUniqueID();
	}
	
	/**
	 * Sets Description to shoe, calls setUniqueID on item class
	 * @param uniqueID your ISBN number
	 */
	public void setDescription(String uniqueID) {
		setUniqueID(uniqueID);
	}
	
}
