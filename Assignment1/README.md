# **Assignment 1** 
Assignment 1 is the cumulative 'halfway point' assignment for COMP1451. Concepts from Lab 1-5 are covered within this assignment. Enumeration is also introduced here.

## Objective ##
The objective of this assignment is to create a general Store class that can be extended to a Bookstore and a shoe store. Both store types must have items that can be analyzed to find details such as shoe type, date of creation/publication, etc. Test cases must be made for assertion of these data types.

## BookStore.java ##
The hierarchy of classes for the Bookstore is:
```
Store -> Bookstore (extends a store)
Item -> Book (extends an item)
```

BookStore.java will add books to the bookstore database. This class has functionality to print:
* All authors over a certain age

* all books by a certain author

* all books written before a certain year

* all books of a genre

* all books written by an author born on a certain year

* all books published on a day of a week

* all books with the biggest percentage markup

* all books written outside an authors speciality


## ShoeStore.java ##
The hierarchy of classes for the Bookstore is:
```
Store -> ShoeStore (extends a store)
Item -> Shoe (extends an item)
```

ShoeStore.java will add shoes to the shoe store database. This class has functionality to print:
* all shoes and designers

* all shoes by a designer

* all shoes made with a certain material

* all shoes designed by a certain designer

* number of shoes designed by a designer (by last name)

* the smallest shoe size

* the total weight (in kg) of all shoes

* all shoes of a certain material designed by a designer

* all shoes not in the store
