/**
 * 
 */
package lab4a;

import java.util.Random;

/**
 * @author Mason Liang
 *
 */
public class Mathematics {
	public static final double PI = 3.14159;
	public static final double ONE_FOOT_TO_KILOMETER_RATIO = 0.0003048;
	
	public Mathematics() {
		
	}
	
	public static double getCircleArea(double raduis) {
		double area;
		area = PI * raduis * raduis;
		return area;
	}
	
	public double getSquareArea(double side) {
		double area;
		area = side * side;
		return area;
	}
	
	public double add(double x, double y) {
		double sum;
		sum = x + y;
		return sum;
	}
	
	public double times(double x, double y) {
		double product;
		product = x * y;
		return product;
	}
	
	public double subtract(double x, double y) {
		double remainder;
		remainder = x - y;
		return remainder;
	}
	
	public int divide(int x, int y) {
		int quotient;
		if(x == 0 || y == 0) {
			return 0;
		}
		quotient = x / y;
		return quotient;
	}
	
	public int absoluteValue(int absoluteValue) {
		int abs = absoluteValue;
		if(absoluteValue < 0) {
			abs = absoluteValue * -1;
		}
		return abs;
		
	}
	
	public Integer doubleTheNumber(Integer x) {
		Integer doubleNumber  = x;
		return doubleNumber * 2;
		
	}
	
	public int getRandomNumberBetweenTenAndTwentyButNotFifteen() {
		Random r = new Random();
		int numbers = r.nextInt(21 - 10) + 10;
		while(numbers == 15 || numbers < 10 || numbers > 20) {
			numbers = r.nextInt(21 - 10) + 10;
		}
		return numbers;
	}
	
	public double convertFeetToKilometers(double feet) {
		double kilometers = feet * ONE_FOOT_TO_KILOMETER_RATIO;
		return kilometers;
	}
}
