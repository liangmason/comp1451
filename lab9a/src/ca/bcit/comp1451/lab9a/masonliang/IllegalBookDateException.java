/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

/**
 * IllegalBookDateException
 * @author liang
 *
 */
public class IllegalBookDateException extends Exception{
	
	public IllegalBookDateException(String m) {
		super(m);
	}
}
