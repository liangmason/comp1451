/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

/**
 * A Biography class
 * @author liang
 *
 */
public final class Biography extends Book{
	private Name subject;

	/**
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param yearPublished
	 * @param subject
	 */
	public Biography(Name firstName, Name lastName, String title, int yearPublished, Name subject) {
		super(firstName, lastName, title, yearPublished);
		setSubject(subject);
	}

	/**
	 * @return the subject
	 */
	public Name getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(Name subject) {
		this.subject = subject;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Biography))
			return false;
		Biography other = (Biography) obj;
		if (getSubject() == null) {
			if (other.getSubject() != null)
				return false;
		} else if (!getSubject().equals(other.getSubject()))
			return false;
		if (getSubject() == other.getSubject()) {
			return true;
		}
		return true;
	}
	
	
}
