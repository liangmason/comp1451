/**
 * 
 */
package ca.bcit.comp1451.lab1a;

/**
 * This class defines a book
 * @author Mason Liang
 * @version 1.0
 *
 */
public class Book {
	private String 	title;
	private Date 	datePublished;

	/**
	 * @param title
	 * @param datePublished
	 */
	public Book(String title, Date datePublished) {
		setTitle (title);
		setDatePublished (datePublished);
	}
	
	/**
	 * @param title
	 * @param datePublished
	 */
	public Book(String title, int year, int month, int day) {
		setTitle (title);
		setDatePublished (new Date (year, month, day));
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		if(!title.equals(null) && !title.isEmpty()) {
			this.title = title;
		}else {
			throw new IllegalArgumentException("invalid title");
		}
	}

	/**
	 * @return the datePublished
	 */
	public Date getDatePublished() {
		return datePublished;
	}

	/**
	 * @param datePublished the datePublished to set
	 */
	public void setDatePublished(Date datePublished) {
		if(datePublished != null) {
			this.datePublished = datePublished;
		}else {
			throw new IllegalArgumentException("invalid date");
		}
	}
	
	
}
