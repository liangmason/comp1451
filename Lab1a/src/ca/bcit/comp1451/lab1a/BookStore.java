/**
 * 
 */
package ca.bcit.comp1451.lab1a;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This class defines a bookstore
 * @author Mason Liang
 * @version 1.0
 *
 */
public class BookStore {
	private ArrayList<Book> books;
	private Book[] arrayOfBooks;




	/**
	 * BookStore constructor
	 */
	public BookStore() {
		books = new ArrayList<>();
		
		books.add(new Book("Book1", 2012, 1, 1));
		books.add(new Book("title of book2", (new Date (2014, 2, 21))));
		books.add(new Book("Book3", 2012, 11, 1));
		books.add(new Book("Book4", 2012, 5, 1));
		books.add(new Book("title of book5", (new Date (2014, 11, 21))));
		
		arrayOfBooks = new Book[5];
		
		arrayOfBooks[0] = new Book("Book1", 2012, 1, 1);
		arrayOfBooks[1] = new Book("title of book2", (new Date (2014, 12, 31)));
		arrayOfBooks[2] = new Book("title of book3", 3424, 12, 11);
		arrayOfBooks[3] = new Book("book4", 8888, 2, 31);
		arrayOfBooks[4] = new Book("book5", 8128, 12, 31);
	}
	
	/**
	 * method that displays your book array and arraylist
	 */
	public void displayBooks() {
		Iterator<Book> it = books.iterator();
		
		
		while(it.hasNext()) {
			Book b = it.next();
			if(b == null){
				System.out.println("no book title");
			}else {
				System.out.println(b.getTitle() + " " + b.getDatePublished().getDate());
			}
		}
		
		for(Book book : arrayOfBooks) {
			if(book == null){
				System.out.println("no book title");
			}else {
				System.out.println(book.getTitle() + " " + book.getDatePublished().getDate());
			}
		}
	}
}
