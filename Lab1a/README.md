# **Lab 1a**
Lab 1a is the introductory lab to COMP1451. The project files contained in this repository will create a bookstore. The user can view details of books within this bookstore.
## Data.java
This file contains a class for dates. It has setters and getters to maintain a Date object’s year, month, and day. This class also contains and minimum and maximum for the day, month, and year.
## Book.java
This file contains a class for a book stored in the bookstore. It has a setters and getters to maintain the title and the date for the book object. This object also utilizes the Date class for setting the publication date.
## Bookstore.java
Bookstore.java will utilize both Book.java and Date.java to create and iterate through all books stored within the bookstore database.
