package ca.bcit.comp1451.lesson07.jasonharrison;

public abstract class Animal {

	public abstract void move();
	public abstract void eat();
	
	public void breathe() {
		System.out.println("oxygen");
	}
}
