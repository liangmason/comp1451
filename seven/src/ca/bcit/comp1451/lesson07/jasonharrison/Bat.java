package ca.bcit.comp1451.lesson07.jasonharrison;

public abstract class Bat extends Animal{
	
	@Override
	public void move() {
		System.out.println("fly");
	}
}
