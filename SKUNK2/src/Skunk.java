import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author liang
 *
 */
public class Skunk {
	private static boolean playing;
	private static int score;
	private static Scanner input = new Scanner(System.in);
	private static int badRolls;
	private static int variable;
	private static Player[] players;
	private static Dice dice;
	private static boolean play;
	private static List<Integer> winners = new ArrayList<Integer>();
	private static int roundNumber;

	public static void playTwoDices() {
		int[] rolledNums = new int[2];

		// Sets playing to true
		playing = true;
		
		/*
		 * Constructs the amount of player objects you want They are stored in the array
		 * "players"
		 */
		System.out.println("How many players would like to play?");
		variable = Integer.parseInt(input.nextLine());
		players = new Player[variable];
		for (int i = 0; i < variable; i++) {
			System.out.println("Player: " + (i + 1) + " what is your name?");
			players[i] = new Player(input.nextLine());
		}
		dice = new Dice();
		
		// Sets round to 1
		roundNumber = 1;

		while (playing && roundNumber < 6) {
			play = true;
			if (roundNumber == 1) {
				System.out.println("Round: S");
			}

			else if (roundNumber == 2) {
				System.out.println("Round: SK");
			}

			else if (roundNumber == 3) {
				System.out.println("Round: SKU");
			}

			else if (roundNumber == 4) {
				System.out.println("Round: SKUN");
			}

			else {
				System.out.println("Round: SKUNK");
			}

			while (play) {
				rolledNums = dice.rollTwoDice();
				System.out.println("You rolled a:");
				System.out.println(rolledNums[0]);
				System.out.println(rolledNums[1]);

				badRolls = dice.badRolls(rolledNums[0], rolledNums[1]);
				if (validStanding() && badRolls > 0) {
					if (validStanding() && badRolls == 2) {
						for (int i = 0; i < players.length; i++) {
							if (players[i].getStanding()) {
								players[i].resetScore();
							}
						}
					}
					play = false;
					System.out.println("Round over!");
				} else {
					score += rolledNums[0] + rolledNums[1];
					for (int i = 0; i < players.length; i++) {
						if (players[i].getStanding()) {
							System.out.println("Would " + players[i].getName() + " like to stay standing? (return true or false)");
							players[i].setStanding(Boolean.parseBoolean(input.nextLine()));
							if (!players[i].getStanding()) {
								players[i].addScore(score);
							}
						}
					}
					playing = validStanding();
				}
			}
			System.out.println(printScoreboard());
	        if (roundNumber < 5){
	            System.out.println("Would you like to play another round? (Reply true or false)");
	            playing = Boolean.parseBoolean(input.nextLine());
	        }
	        
	        roundNumber++;
	        score = 0;
	        badRolls = 0;

	        for (int i = 0; i < players.length; i++){
	            players[i].setStanding(true);
	        }
		}
		winners.add(0);
		for (int i = 1; i < players.length; i++) {
			if (players[i].getScore() == players[(winners.get(0)).intValue()].getScore()) {
				winners.add(i);
			}

			else if (players[i].getScore() > players[(winners.get(0)).intValue()].getScore()) {
				winners.clear();
				winners.add(i);
			}
		}

		if (winners.size() > 1) {
			System.out.print("\n It's a tie between ");
			for (int i = 0; i < winners.size(); i++) {
				System.out.print(players[(winners.get(i)).intValue()].getName() + " ");
				if (i != winners.size() - 1) {
					System.out.print("and ");
				}
			}
			System.out.print("with a score of " + players[(winners.get(0)).intValue()].getScore() + "!");
		} else {
			System.out.print("\n" + players[(winners.get(0)).intValue()].getName() + " is the winner with a score of "
					+ players[(winners.get(0)).intValue()].getScore() + "!");
		}

		System.out.println("\nThanks for playing! \n");
        
	}

	public static boolean validStanding() {
		for (Player p : players) {
			if (p.getStanding()) {
				return true;
			}
		}
		return false;
	}
	
	public static String printScoreboard(){
	       String output = "";
	       output += "-------------------------\n";
	       for(int i = 0; i < players.length; i++){
	           output += players[i].getName() + ": " + players[i].getScore();
	           output += "\n";
	       }
	       output += "-------------------------\n";

	       return output;
	   }
}
