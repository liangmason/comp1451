import java.util.Random;

/**
 * Dice class for Skunk
 * 
 * @author Mason Liang
 * @version 1.0
 */
public class Dice {
	private Random r;
	private int[] diceRolled;

	/**
	 * Dice constructor, creates a size three arrays to store dice rolls. Can
	 * probably do without
	 */
	public Dice() {
		diceRolled = new int[3];
		r = new Random();
	}

	/**
	 * Rolls two dices
	 * 
	 * @return Array of two dice rolls
	 */
	public int[] rollTwoDice() {
		diceRolled = new int[2];
		diceRolled[0] = (r.nextInt(6) + 1);
		diceRolled[1] = (r.nextInt(6) + 1);

		return diceRolled;
	}

	/**
	 * Rolls three dice
	 * 
	 * @return Array of three dice rolls
	 */
	public int[] rollThreeDice() {
		diceRolled = new int[3];
		Random r = new Random();

		diceRolled[0] = (r.nextInt(6) + 1);
		diceRolled[1] = (r.nextInt(6) + 1);
		diceRolled[2] = (r.nextInt(6) + 1);

		return diceRolled;
	}

	public int badRolls(int dice1, int dice2) {
		if(dice1 == 1) {
			return 1;
		}
		
		if(dice2 == 1) {
			return 1;
		}
		
		if(dice1 + dice2 == 2) {
			return 2;
		}
		
		return 0;
	}

}