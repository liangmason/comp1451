package ca.bcit.comp1451.skunk.masonliang;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Game of skunk
 * @author Mason Liang
 * @version 0.1
 *
 */
public class Skunk {
	private static Player[] players;
	private static List<Integer> winners = new ArrayList<Integer>();
	private static int round = 1;
	private static boolean playRound;
	private static boolean play;
	private static int roundScore;
	private static int badRoll;
	
	private static final int FIRST_ROUND 	= 1;
	private static final int SECOND_ROUND 	= 2;
	private static final int THIRD_ROUND 	= 3;
	private static final int FORTH_ROUND 	= 4;
	private static final int FIFTH_ROUND 	= 5;
	
	private static final int FIRST_DICE 	= 0;
	private static final int SECOND_DICE 	= 1;
	private static final int THIRD_DICE 	= 2;
	
	public static final int ZERO 		= 0;
	public static final int ONE 		= 1;
	public static final int TWO 		= 2;
	public static final int THREE 		= 3;
	public static final int HUNDRED 	= 100;
	
	/**
	 * Skunk game of 2 dices
	 */
	public void playSkunkTwoDices() {
		// TODO: Make computer player
		playRound = true;
		int[] diceRolled;
		Dice dice = new Dice(TWO);

		System.out.println("SKUNK! Enter Q to quit.");

		//
		System.out.println("How many players?");
		Scanner s = new Scanner(System.in);
		int playersNums = Integer.parseInt(s.nextLine());
		players = new Player[playersNums];
		for (int i = ZERO; i < playersNums; i++) {
			System.out.println("Player: " + (i + ONE) + " enter your name.");
			players[i] = new Player(s.nextLine()); // TODO: Make it so that you can't have same name
		}

		// Prints out what round of SKUNK it is
		while (playRound && round <= FIFTH_ROUND) {
			roundScore = ZERO;
			play = true;
			if (round == FIRST_ROUND) {
				System.out.println("S");
			} else if (round == SECOND_ROUND) {
				System.out.println("SK");
			} else if (round == THIRD_ROUND) {
				System.out.println("SKU");
			} else if (round == FORTH_ROUND) {
				System.out.println("SKUN");
			} else {
				System.out.println("SKUNK");
			}
			// Runs if there are players standing up
			while (play) {
				diceRolled = dice.rollTwoDice();
				roundScore = roundScore + diceRolled[FIRST_DICE] + diceRolled[SECOND_DICE];
				System.out.println("Rolled a " + diceRolled[FIRST_DICE] + " and a " + diceRolled[SECOND_DICE]);

				badRoll = dice.badRolls(diceRolled[FIRST_DICE], diceRolled[SECOND_DICE]);
				// Checks for 1s in the dice rolls.
				if (badRoll > ZERO && standing()) { //TODO: Make it so that if all players are standing, it doesn't run
					if (badRoll == 2 && standing()) {
						for (int i = ZERO; i < players.length; i++) {
							if (players[i].getStandingUp()) {
								players[i].resetScore();
							}
						}
					}
					roundScore = ZERO;
					play = false;
					System.out.println("Round " + round + " is over, players standing lost their points for this round.");
				} else {
					System.out.println("Potential points for Round " + round + " is " + roundScore);

					for (int i = ZERO; i < players.length; i++) {
						if (players[i].getStandingUp()) {
							System.out.println(
									"Would " + players[i].getName() + " like to stay standing? (return true or false)");
							players[i].setStandingUp(Boolean.parseBoolean(s.nextLine()));
							if (!players[i].getStandingUp()) {
								players[i].addScore(roundScore);
							}
						}
					}
					play = standing();
				}
			}
			System.out.println(printScoreboard());
			if (round < FIFTH_ROUND) {
				System.out.println("Would you like to play another round? (True or false)");
				playRound = Boolean.parseBoolean(s.nextLine());
			}

			System.out.println(" ");
			round++;
			roundScore = ZERO;
			badRoll = ZERO;

			// Set all players to standing
			for (int i = ZERO; i < players.length; i++) {
				players[i].setStandingUp(true);
			}
		}
		// Adds players to arraylist, runs through all players score and store it in
		// arraylist
		winners.add(ZERO);
		for (int i = ONE; i < players.length; i++) {
			if (players[i].getScore() == players[(winners.get(ZERO)).intValue()].getScore()) {
				winners.add(i);
			}

			else if (players[i].getScore() > players[(winners.get(ZERO)).intValue()].getScore()) {
				winners.clear(); // Clears score if higher score found
				winners.add(i);
			}
		}

		if (winners.size() > ONE) {
			System.out.print("\n It's a tie between ");
			for (int i = ZERO; i < winners.size(); i++) {
				System.out.print(players[(winners.get(i)).intValue()].getName() + " ");
				if (i != winners.size() - ONE) {
					System.out.print("and ");
				}
			}
			System.out.print("with a score of " + players[(winners.get(ZERO)).intValue()].getScore() + "!");
		} else {
			System.out.print("\n" + players[(winners.get(ZERO)).intValue()].getName() + " is the winner with a score of "
					+ players[(winners.get(ZERO)).intValue()].getScore() + "!");
		}

		System.out.println("\n Thanks for playing! \n");
	}
	
	/**
	 * Skunk game of three dices
	 */
	public void playSkunkThreeDices() {
		// TODO: Make computer player
		playRound = true;
		int[] diceRolled;
		Dice dice = new Dice(THREE);

		System.out.println("SKUNK! Enter Q to quit.");

		//
		System.out.println("How many players?");
		Scanner s = new Scanner(System.in);
		int playersNums = Integer.parseInt(s.nextLine());
		players = new Player[playersNums];
		for (int i = ZERO; i < playersNums; i++) {
			System.out.println("Player: " + (i + ONE) + " enter your name.");
			players[i] = new Player(s.nextLine()); // TODO: Make it so that you can't have same name
		}

		// Prints out what round of SKUNK it is
		while (playRound && round <= FIFTH_ROUND) {
			roundScore = ZERO;
			play = true;
			if (round == FIRST_ROUND) {
				System.out.println("S");
			} else if (round == SECOND_ROUND) {
				System.out.println("SK");
			} else if (round == THIRD_ROUND) {
				System.out.println("SKU");
			} else if (round == FORTH_ROUND) {
				System.out.println("SKUN");
			} else {
				System.out.println("SKUNK");
			}
			// Runs if there are players standing up
			while (play) {
				diceRolled = dice.rollThreeDice();
				roundScore = roundScore + diceRolled[FIRST_DICE] + diceRolled[SECOND_DICE] + diceRolled[THIRD_DICE];
				System.out.println("Rolled a " + diceRolled[FIRST_DICE] + " a " + diceRolled[1] + " and a " + diceRolled[THIRD_DICE]);
				// Checks for 1s in the dice roll
				badRoll = dice.badRolls(diceRolled[FIRST_DICE], diceRolled[SECOND_DICE], diceRolled[THIRD_DICE]);
				if (badRoll > ZERO && standing()) { //TODO: See rollTwoDices()
					if (badRoll == TWO && standing()) {
						for (int i = ZERO; i < players.length; i++) {
							if (players[i].getStandingUp()) {
								roundScore = ZERO;
							}
						}
					}
					if (badRoll == 3 && standing()) {
						for (int i = ZERO; i < players.length; i++) {
							if (players[i].getStandingUp()) {
								players[i].addScore(HUNDRED);
							}
						}
					}
					play = false;
					System.out.println("Round " + round + " is over, players lost their points for this round.");
				} else {
					System.out.println("Potential points for Round " + round + " is " + roundScore);

					for (int i = ZERO; i < players.length; i++) {
						if (players[i].getStandingUp()) {
							System.out.println(
									"Would " + players[i].getName() + " like to stay standing? (return true or false)");
							players[i].setStandingUp(Boolean.parseBoolean(s.nextLine()));
							if (!players[i].getStandingUp()) {
								players[i].addScore(roundScore);
							}
						}
					}
					play = standing();
				}
			}
			System.out.println(printScoreboard());
			if (round < FIFTH_ROUND) {
				System.out.println("Would you like to play another round? (True or false)");
				playRound = Boolean.parseBoolean(s.nextLine());
			}

			System.out.println(" ");
			round++;
			roundScore = ZERO;
			badRoll = ZERO;

			// Set all players to standing
			for (int i = ZERO; i < players.length; i++) {
				players[i].setStandingUp(true);
			}
		}
		// Adds players to arraylist, runs through all players score and store it in
		// arraylist
		winners.add(ZERO);
		for (int i = ONE; i < players.length; i++) {
			if (players[i].getScore() == players[(winners.get(ZERO)).intValue()].getScore()) {
				winners.add(i);
			}

			else if (players[i].getScore() > players[(winners.get(ZERO)).intValue()].getScore()) {
				winners.clear(); // Clears score if higher score found
				winners.add(i);
			}
		}

		if (winners.size() > 1) {
			System.out.print("\n It's a tie between ");
			for (int i = ZERO; i < winners.size(); i++) {
				System.out.print(players[(winners.get(i)).intValue()].getName() + " ");
				if (i != winners.size() - ONE) {
					System.out.print("and ");
				}
			}
			System.out.print("with a score of " + players[(winners.get(ZERO)).intValue()].getScore() + "!");
		} else {
			System.out.print("\n" + players[(winners.get(ZERO)).intValue()].getName() + " is the winner with a score of "
					+ players[(winners.get(ZERO)).intValue()].getScore() + "!");
		}

		System.out.println("\n Thanks for playing! \n");
	}

	/**
	 * @return boolean true or false if all players is standing
	 */
	private static boolean standing() {
		for (int i = ZERO; i < players.length; i++) {
			if (players[i].getStandingUp()) {
				return true;
			}
		}
		return false;
	}
	/**
	 * Scoreboard for game
	 * @return String of player and score
	 */
	private static String printScoreboard() {
		String output = "";
		output += "-----------------------\n";
		for (int i = ZERO; i < players.length; i++) {
			output += players[i].getName() + ": " + players[i].getScore();
			output += "\n";
		}
		output += "-----------------------\n";

		return output;
	}
}
