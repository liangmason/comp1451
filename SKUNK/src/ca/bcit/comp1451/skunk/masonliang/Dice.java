package ca.bcit.comp1451.skunk.masonliang;

import java.util.Random;

/**
 * Dice class for Skunk
 * 
 * @author Mason Liang
 * @version 1.0
 */
public class Dice {
	private Random r;
	private int[] diceRolled;
	private int numberOfDices;
	
	private static final int DICE_1 = 0;
	private static final int DICE_2 = 1;
	private static final int DICE_3 = 2;
	
	private static final int MIN_DICE = 1;
	private static final int MAX_DICE = 6;
	
	
	/**
	 * Dice constructor, creates a size three arrays to store dice rolls. Can
	 * probably do without
	 */
	public Dice(int numberOfDices) {
		diceRolled = new int[numberOfDices];
		r = new Random();
	}

	/**
	 * Rolls two dices
	 * 
	 * @return Array of two dice rolls
	 */
	public int[] rollTwoDice() {
		diceRolled = new int[Skunk.TWO];
		diceRolled[DICE_1] = (r.nextInt(MAX_DICE) + MIN_DICE);
		diceRolled[DICE_2] = (r.nextInt(MAX_DICE) + MIN_DICE);

		return diceRolled;
	}

	/**
	 * Rolls three dice
	 * 
	 * @return Array of three dice rolls
	 */
	public int[] rollThreeDice() {
		diceRolled = new int[Skunk.THREE];
		Random r = new Random();

		diceRolled[DICE_1] = (r.nextInt(MAX_DICE) + MIN_DICE);
		diceRolled[DICE_2] = (r.nextInt(MAX_DICE) + MIN_DICE);
		diceRolled[DICE_3] = (r.nextInt(MAX_DICE) + MIN_DICE);

		return diceRolled;
	}
	
	/**
	 * Checks 2 diceroll for 1s
	 * 
	 * @param dice1 checks first dice
	 * @param dice2 checks second dice
	 */
	public int badRolls(int dice1, int dice2) {
		if(dice1 == Skunk.ONE) {
			return Skunk.ONE;
		}
		
		if(dice2 == Skunk.ONE) {
			return Skunk.ONE;
		}
		
		if(dice1 + dice2 == Skunk.TWO) {
			return Skunk.TWO;
		}
		
		return Skunk.ZERO;
	}
	
	/**
	 * Checks 3 dice roll for 1s
	 * 
	 * @param dice1 checks first dice
	 * @param dice2 checks second dice
	 * @param dice3 checks third dice
	 */
	public int badRolls(int dice1, int dice2, int dice3) {
		if(dice1 == Skunk.ONE) {
			return Skunk.ONE;
		}
		
		if(dice2 == Skunk.ONE) {
			return Skunk.ONE;
		}
		
		if(dice3 == Skunk.ONE) {
			return Skunk.ONE;
		}
		
		if(dice1 + dice2 == Skunk.TWO) {
			return Skunk.TWO;
		}
		
		if(dice1 + dice3 == Skunk.TWO) {
			return Skunk.TWO;
		}
		
		if(dice2 + dice3 == Skunk.TWO) {
			return Skunk.TWO;
		}
		
		if(dice1 + dice2 + dice3 == Skunk.THREE) {
			return Skunk.TWO;
		}
		
		if(dice1 == dice2 && dice1 == dice3) {
			return Skunk.THREE;
		}
		
		return Skunk.ZERO;
	}

}
