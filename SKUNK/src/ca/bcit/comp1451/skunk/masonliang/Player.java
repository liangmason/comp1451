/**
 * 
 */
package ca.bcit.comp1451.skunk.masonliang;

/**
 * Player class for Skunk game
 * @author Mason Liang
 *
 */
public class Player {
	private String name;
	private boolean standingUp;
	private int score;
	
	private static final int MIN_SCORE = 0;
	
	/**
	 * Creates a new player.
	 * @param name
	 * @param standingUp
	 */
	Player(String name){
		setName(name);
		standingUp = true;
		score = MIN_SCORE;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @return the name of player
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the standingUp position of player
	 */
	public boolean getStandingUp() {
		return standingUp;
	}
	
	/**
	 * Add points to score
	 * @param points Player score
	 */
	public void addScore(int score) {
		this.score += score;
	}
	
	/**
	 * Reset score back to 0 
	 */
	public void resetScore() {
		score = MIN_SCORE;
	}

	/**
	 * @param sets name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param sets if player is standing up
	 */
	public void setStandingUp(boolean standingUp) {
		this.standingUp = standingUp;
	}
	
	
}
