/**
 * 
 */
package ca.bcit.comp1451.lab7a;

/**
 * @author A00993782
 *
 */
public abstract class iPad extends iDevice{
	private boolean hasACase;
	private String operatingSystemVersion;
	
	/**
	 * @param purpose
	 * @param hasACase
	 * @param oSver
	 */
	public iPad(boolean hasACase, String operatingSystemVersion) {
		super("learning");
		setHasACase(hasACase);
		setOperatingSystemVersion(operatingSystemVersion);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((operatingSystemVersion == null) ? 0 : operatingSystemVersion.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof iPad))
			return false;
		iPad other = (iPad) obj;
		if (operatingSystemVersion == null) {
			if (other.operatingSystemVersion != null)
				return false;
		} else if (!operatingSystemVersion.equals(other.operatingSystemVersion))
			return false;
		if(operatingSystemVersion.equalsIgnoreCase(other.operatingSystemVersion)) {
			return true;
		}
		return true;
	}

	/**
	 * @return the hasACase
	 */
	public boolean isHasACase() {
		return hasACase;
	}

	/**
	 * @return the oSver
	 */
	public String getOperatingSystemVersion() {
		return operatingSystemVersion;
	}

	/**
	 * @param hasACase the hasACase to set
	 */
	public void setHasACase(boolean hasACase) {
		this.hasACase = hasACase;
	}

	/**
	 * @param oSver the oSver to set
	 */
	public void setOperatingSystemVersion(String operatingSystemVersion) {
		this.operatingSystemVersion = operatingSystemVersion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "iPad [hasACase= " + hasACase + ", operatingSystemVersion= " + operatingSystemVersion + ", purpose= "
				+ getPurpose() +  "]";
	}
	
	/**
	 * prints out instance variable details
	 */
	@Override
	public void printDetails() {
		System.out.println("Has a case: " + isHasACase() + ". OS version: " + getOperatingSystemVersion());
	}
	
	

	
	
}
