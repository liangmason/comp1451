/**
 * 
 */
package ca.bcit.comp1451.lab7a;

/**
 * @author A00993782
 *
 */
public abstract class iPod extends iDevice{
	private int numberOfSongs;
	private double maxVolumeinDb;
	
	/**
	 * @param numberOfSongs
	 * @param maxVolumeinDb
	 */
	public iPod(int numberOfSongs, double maxVolumeinDb) {
		super("music");
		setNumberOfSongs(numberOfSongs);
		setMaxVolumeinDb(maxVolumeinDb);
	}
	
	/**
	 * @return the numberOfSongs
	 */
	public int getNumberOfSongs() {
		return numberOfSongs;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + numberOfSongs;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof iPod))
			return false;
		iPod other = (iPod) obj;
		if (numberOfSongs != other.numberOfSongs)
			return false;
		if (numberOfSongs == other.numberOfSongs)
			return true;
		return true;
	}

	/**
	 * @return the maxVolumeinDb
	 */
	public double getMaxVolumeinDb() {
		return maxVolumeinDb;
	}
	
	/**
	 * @param numberOfSongs the numberOfSongs to set
	 */
	public void setNumberOfSongs(int numberOfSongs) {
		this.numberOfSongs = numberOfSongs;
	}
	
	/**
	 * @param maxVolumeinDb the maxVolumeinDb to set
	 */
	public void setMaxVolumeinDb(double maxVolumeinDb) {
		this.maxVolumeinDb = maxVolumeinDb;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "iPod [numberOfSongs= " + numberOfSongs + ", maxVolumeinDb= " + maxVolumeinDb + ", purpose= "
				+ getPurpose() + "]";
	}
	
	/**
	 * prints out instance variable details
	 */
	@Override
	public void printDetails() {
		System.out.println("Number of songs: " + getNumberOfSongs() + ". Max Volume: " + getMaxVolumeinDb());
	}
	
	
	
}
