# **Lab 7**
Lab 7 is formal introductory lab to abstract classes and comparable interfaces (used to order the objects of the user-defined class). Apple devices (such as an iPhone, iPad, etc...) are used as an example for these concepts.

## iDevice.java ##
This abstract class is used as the starting base for extension and implementation in subclasses of an iDevice (iPhone, iPad, etc...). The iDevice class will contain a hashCode() function that returns a result after utilizing a prime number for computation. The equals (==) method is overridden to work with instances of iDevice classes. There is also a setter and getter to maintain a "purpose" (string) of the class (what the iDevice is for).

## iPhone.java ##
iPhone.java is an abstract class that extends the iDevice class. This class represents a general iPhone Apple device and has parameters for the carrier and number of minutes the device has. The class also contains setters and getters for the parameters and can print all of these details.

## iPod.java ##
iPod.java is an abstract class that extends the iDevice class. This class represents a general iPod Apple device by having parameters for the number of songs and max volume in decibels for the device. The class also contains setters and getters for the parameters and can print all of these details. For example, callng printDetails() for an iPod device with 5 songs and a dB limit of 50 will print:
```
Number of songs: 5. Max Volume: 50
```

## iPad.java ##
The iPad.java is an abstract class that extends the iDevice class. This class represents a general iPad Apple device and has 2 unique parameters: a boolean for having a case and the iOS version for the device.

## iPhoneSeven.java ##
The iPhoneSeven.java class is an abstract class that extends the iPhone class. It has 2 special parameters that can be modified by setters and getters: a boolean for having a 'high resolution camera' and an integer for 'gigabytes of memory'.
