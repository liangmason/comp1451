package ca.bcit.comp1451.jasonharrison;

public class School {

	public static void main(String[] args) {
		
		try {
			Person p = new Person(2000);
			try{
				p.setWeightKg(100);
			}catch(IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
			return; // STILL calls finally code
		}catch(IllegalYearBornException e) {
			System.out.println("deal with invalid year born");
			System.out.println(e.getMessage());
		}catch(Exception e) {
			System.out.println("?????");
			System.out.println(e.getClass());
		}finally {
			System.out.println("FINALLY!!!");
		}
	}
	
}
