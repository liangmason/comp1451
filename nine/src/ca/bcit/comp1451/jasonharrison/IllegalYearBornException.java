package ca.bcit.comp1451.jasonharrison;

public class IllegalYearBornException
	extends Exception{

	public IllegalYearBornException(String m) {
		super(m);
	}
}
