package ca.bcit.comp1451.quiz4.masonliang;

public class Book {
	private int    yearPublished;
	private String title;
	private String firstName;
	private String lastName;
	
	public static final String FIRST_BOOK_PUBLISHED_YEAR = "1455";
	
	/**
	 * @param yearPublished
	 * @param title
	 * @param firstName
	 * @param lastName
	 */
	public Book(int yearPublished, String title, String firstName, String lastName) {
		setYearPublished (yearPublished);
		setTitle (title);
		setFirstName (firstName);
		setLastName (lastName);
	}
	
	public String getAuthorInitials() {
		String firstInitials = getFirstName().substring(0, 1).toUpperCase();
		String lastInitials = getLastName().substring(0, 1).toUpperCase();
		return String.format(firstInitials + "." + lastInitials + ".");	
	}
	
	public int getYearPublished() {
		return yearPublished;
	}

	public String getTitle() {
		String title = this.title;
		String authorTitle = String.format(title.toUpperCase().substring(0 , 1) + title.substring(1).toLowerCase());
		return authorTitle;
		
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public String getAuthorFirstName() {
		String firstName = getFirstName();
		String authorFirstName = String.format(firstName.toUpperCase().substring(0 , 1) + firstName.substring(1).toLowerCase());
		return authorFirstName;
		
	}

	public String getAuthorLastName() {
		String lastName = getLastName();
		String authorLastName = String.format(lastName.toLowerCase().substring(0 , 5) + lastName.substring(5).toUpperCase());
		return authorLastName;
	}
	
	public static String getActivity() {
		return String.format("Reading");
	}

	public void setYearPublished(int yearPublished) {
		this.yearPublished = yearPublished;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	public String getDetails() {
		return String.format(getTitle().toUpperCase() + " WAS PUBLISHED BY " + getAuthorFirstName().toUpperCase() + " " + getAuthorLastName().toUpperCase() + " IN " + getYearPublished() + "!");
	}
}
