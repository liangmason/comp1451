package ca.bcit.comp1451.lab8.masonliang;

/**
 * Employable interface
 * @author Mason
 *
 */
public interface Employable {
	public String getDressCode();
	public boolean isPaidSalary();
	public boolean postSecondaryEducationRequired();
	public String getWorkVerb();
	
	/**
	 * @return employable gets paid
	 */
	default public boolean getsPaid() {
		return true;
	}
}
