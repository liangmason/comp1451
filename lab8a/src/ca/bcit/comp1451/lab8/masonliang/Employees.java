/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Mason
 *
 */
public class Employees {
	private ArrayList<HockeyPlayer> hockeyPlayers;
	private ArrayList<Professor> professors;
	private ArrayList<Parent> parents;
	private ArrayList<GasStationAttendant> gasStationAttendents;
	
	private static final int WAYNE_GREZKY_GOALS = 894;
	private static final int PAVEL_BLUE_GOALS 	= 437;
	private static final int BRET_BURE_GOALS 	= 1;
	private static final int NO_GOALS 			= 0;
	
	private static final int TIGER_WOODS_TIME_WITH_KIDS_PER_WEEK 	= 1;
	private static final int SUPER_MOM_TIME_WITH_KIDS_PER_WEEK 		= 168;
	private static final int LAZY_LARRY_TIME_WITH_KIDS_PER_WEEK		= 20;
	private static final int EX_HAUSTED_TIME_WITH_KIDS_PER_WEEK 	= 168;
	private static final int SUPER_DAD_TIME_WITH_KIDS_PER_WEEK 		= 167;
	
	private static final int JOE_SMITH_STOLEN_DOLLARS 			= 10;
	private static final int TONY_BALONEY_STOLEN_DOLLARS 		= 100;
	private static final int BENJAMIN_FRANKLIN_STOLEN_DOLLARS 	= 100;
	private static final int MARY_FAIRY_STOLEN_DOLLARS 			= 101;
	private static final int BEE_SEE_STOLEN_DOLLARS 			= 1;
	

	/**
	 * @param employees
	 */
	public Employees() {
		hockeyPlayers 		 = new ArrayList<>();
		professors 			 = new ArrayList<>();
		parents 			 = new ArrayList<>();
		gasStationAttendents = new ArrayList<>();
		
		// Adds 5 Hockey Players
		hockeyPlayers.add(new HockeyPlayer("Wayne Gretzky", WAYNE_GREZKY_GOALS));
		hockeyPlayers.add(new HockeyPlayer("Who Ever", NO_GOALS));
		hockeyPlayers.add(new HockeyPlayer("Bret Bure", BRET_BURE_GOALS));
		hockeyPlayers.add(new HockeyPlayer("Pavel Blue", PAVEL_BLUE_GOALS));
		hockeyPlayers.add(new HockeyPlayer("Jason Harrison", NO_GOALS));
		
		// Adds 5 Processor
		professors.add(new Professor("Albert Einstein", "Physics"));
		professors.add(new Professor("Jason Harrison", "Computer Systems"));
		professors.add(new Professor("Richard Feymond", "Physics"));
		professors.add(new Professor("BCIT Instructor", "Computer Systems"));
		professors.add(new Professor("Kurt Godel", "Logic"));
		
		// Adds 5 Parents
		parents.add(new Parent("Tiger Woods", TIGER_WOODS_TIME_WITH_KIDS_PER_WEEK));
		parents.add(new Parent("Super Mom", SUPER_MOM_TIME_WITH_KIDS_PER_WEEK));
		parents.add(new Parent("Lazy Larry", LAZY_LARRY_TIME_WITH_KIDS_PER_WEEK));
		parents.add(new Parent("Ex Hausted", EX_HAUSTED_TIME_WITH_KIDS_PER_WEEK));
		parents.add(new Parent("Super Dad", SUPER_DAD_TIME_WITH_KIDS_PER_WEEK));
		
		// Adds 5 Gas station attendants
		gasStationAttendents.add(new GasStationAttendant("Joe Smith", JOE_SMITH_STOLEN_DOLLARS));
		gasStationAttendents.add(new GasStationAttendant("Tony Baloney", TONY_BALONEY_STOLEN_DOLLARS));
		gasStationAttendents.add(new GasStationAttendant("Benjamin Franklin", BENJAMIN_FRANKLIN_STOLEN_DOLLARS));
		gasStationAttendents.add(new GasStationAttendant("Mary Fairy", MARY_FAIRY_STOLEN_DOLLARS));
		gasStationAttendents.add(new GasStationAttendant("Bee See", BEE_SEE_STOLEN_DOLLARS));
		
		Collections.sort(hockeyPlayers);
		Collections.sort(professors);
		Collections.sort(parents);
		Collections.sort(gasStationAttendents);
		
		
	}
	
	
}
	

