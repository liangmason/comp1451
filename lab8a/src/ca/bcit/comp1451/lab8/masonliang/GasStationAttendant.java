/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

/**
 * This class defines a gas station attendant employee
 * @author Mason
 *
 */
public class GasStationAttendant extends Employee implements Comparable<GasStationAttendant>{
	private double numberOfDollarsStolenPerDay;
	private static final double OVER_TIME_PAY_RATE = 1.5;
	
	/**
	 * @param numberOfDollarsStolenPerDay
	 */
	public GasStationAttendant(String name, double numberOfDollarsStolenPerDay) {
		super(name);
		setNumberOfDollarsStolenPerDay(numberOfDollarsStolenPerDay);
	}

	/**
	 * @return the numberOfDollarsStolenPerDay
	 */
	public double getNumberOfDollarsStolenPerDay() {
		return numberOfDollarsStolenPerDay;
	}

	/**
	 * @param numberOfDollarsStolenPerDay the numberOfDollarsStolenPerDay to set
	 */
	public void setNumberOfDollarsStolenPerDay(double numberOfDollarsStolenPerDay) {
		this.numberOfDollarsStolenPerDay = numberOfDollarsStolenPerDay;
	}

	/**
	 * @return Dress code
	 */
	@Override
	public String getDressCode() {
		return "uniform";
	}
	
	/**
	 * @return Is the employee paid salary?
	 */
	@Override
	public boolean isPaidSalary() {
		return false;
	}
	
	/**
	 * @return Does the position require a post secondary education?
	 */
	@Override
	public boolean postSecondaryEducationRequired() {
		return false;
	}
	
	/**
	 * @return Work
	 */
	@Override
	public String getWorkVerb() {
		return "care";
	}
	
	/**
	 * @return Overtime rate
	 */
	@Override
	public double getOverTimePayRate() {
		return OVER_TIME_PAY_RATE;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(numberOfDollarsStolenPerDay);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof GasStationAttendant))
			return false;
		GasStationAttendant other = (GasStationAttendant) obj;
		if (Double.doubleToLongBits(numberOfDollarsStolenPerDay) != Double
				.doubleToLongBits(other.numberOfDollarsStolenPerDay))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(GasStationAttendant o) {
		return (int)(this.getNumberOfDollarsStolenPerDay() - o.getNumberOfDollarsStolenPerDay());
	}
}
