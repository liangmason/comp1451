/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

/**
 * This class defines a employee
 * @author Mason
 *
 */
public abstract class Employee implements Employable{
	private String name;
	public abstract double getOverTimePayRate();
	
	/**
	 * @param name of employee
	 */
	public Employee(String name) {
		setName(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
