/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

/**
 * This class define a processor employee
 * @author Mason
 */
public class Professor extends Employee implements Comparable<Professor>{
	private String teachingMajor;
	private static final double OVER_TIME_PAY_RATE = 2.0;
	
	/**
	 * @param name
	 * @param teachingMajor
	 */
	public Professor(String name, String teachingMajor) {
		super(name);
		setTeachingMajor(teachingMajor);
	}

	/**
	 * @return the teachingMajor
	 */
	public String getTeachingMajor() {
		return teachingMajor;
	}

	/**
	 * @param teachingMajor the teachingMajor to set
	 */
	public void setTeachingMajor(String teachingMajor) {
		this.teachingMajor = teachingMajor;
	}

	/**
	 * @return Dress code
	 */
	@Override
	public String getDressCode() {
		return "fancy";
	}
	
	/**
	 * @return Is the employee paid salary?
	 */
	@Override
	public boolean isPaidSalary() {
		return true;
	}
	
	/**
	 * @return Does the job require a post secondary education?
	 */
	@Override
	public boolean postSecondaryEducationRequired() {
		return true;
	}
	
	/**
	 * @return work
	 */
	@Override
	public String getWorkVerb() {
		return "teach";
	}
	
	/**
	 * @return Overtime rate
	 */
	@Override
	public double getOverTimePayRate() {
		return OVER_TIME_PAY_RATE;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((teachingMajor == null) ? 0 : teachingMajor.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Professor))
			return false;
		Professor other = (Professor) obj;
		if (teachingMajor == null) {
			if (other.teachingMajor != null)
				return false;
		} else if (!teachingMajor.equals(other.teachingMajor))
			return false;
		return true;
	}
	
	/**
	 * Override compare to so that computer sciences are better than any other majors 
	 */
	@Override
	public int compareTo(Professor o) {
		if(this.getTeachingMajor().equalsIgnoreCase("Computer Science")) {
			return 1;
		}else {
			return 0;
		}
	}
}
