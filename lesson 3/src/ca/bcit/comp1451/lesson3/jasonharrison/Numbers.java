package ca.bcit.comp1451.lesson3.jasonharrison;

import java.util.HashMap;
import java.util.Set;

public class Numbers 
{
	HashMap<String, Integer> numbers;

	public Numbers() 
	{
		numbers = new HashMap<>(); 
		
		numbers.put("one", 1);
		numbers.put("five", 5);
		numbers.put("seven", 7);
		numbers.put(null, 8);
		numbers.put("nine", 0);
		
		numbers.remove("seven");
		
		Set<String> num = numbers.keySet();
		
		for (String key : num) {
			System.out.println(numbers.get(key));
		}
	}
}
