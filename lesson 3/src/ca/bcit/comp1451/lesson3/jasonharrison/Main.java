package ca.bcit.comp1451.lesson3.jasonharrison;


import java.util.HashMap;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		IMDB i = new IMDB();
		
		i.displayAllTitles();
		
		// key is Car
		// value is a Book
		HashMap<Car, CarBook> books = new HashMap<>();
		
		books.put(new Car("chevrolet", "corvette"),
				  new CarBook("american muscle cars"));
		
		books.put(new Car("dodge", "hellcat"),
				  new CarBook("the best of dodge"));
		
		Car c = new Car("lamborghini", "aventador");
		
		books.put(c,
				  new CarBook("italian cars"));
	
		System.out.println("the best book about " + c.getMake() + " " +
		                   c.getModel() + " is " + books.get(c).getTitle());
		 
		books.remove(c);
		
		Set<Car> keys = books.keySet();
		
		for(Car key : keys) {
			System.out.println(books.get(key).getTitle());
		}
		
		System.out.println("number of books: " + books.size());
	}
}
