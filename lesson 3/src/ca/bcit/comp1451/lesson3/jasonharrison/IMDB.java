package ca.bcit.comp1451.lesson3.jasonharrison;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class IMDB {
	private HashMap<String, String> titles;
	
	public IMDB(){
		titles = new HashMap<>();
		
		titles.put("tt0107048","groundhog day");
		titles.put("tt0133093","the matrix");
		titles.put(null, "the life of keir forster");
		titles.put("tt123", null);
	}
	
	public void displayAllTitles(){
		
		Set<String> keys = titles.keySet();
		Iterator<String> it = keys.iterator();
		
		while(it.hasNext()){
			String key = it.next();
			
			System.out.println(titles.get(key));
		}
	}
	
	public void display() {		
		Set<String> keys = titles.keySet();
		
		for(String k : keys) {
			System.out.println(titles.get(k));
		}
	}
}
