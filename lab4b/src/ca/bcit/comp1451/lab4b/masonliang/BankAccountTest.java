package ca.bcit.comp1451.lab4b.masonliang;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.jupiter.api.Test;

class BankAccountTest {
	private  BankAccount b1;
	private  BankAccount b2;
	
	@Before
	public void setUp() {
		System.out.println("Setting up");
	}
	
	@Test
	public void testConstructor() {
		new BankAccount("Mason", "Liang", 1234, 5000, 2016);
		new BankAccount("JaCkie", "cheN", 7224, 540000, 1988);
		new BankAccount("John", "Smith", 4414, 50200, 2001);
		new BankAccount("Micheal", "JoRDan", 3456, 19388, 1990);
	}
	
	@Test
	public void testConstructorFirstNameNull() {
		try {
			new BankAccount(null, "nsfgds", 1245, 0.0, 2133);
			fail("null first name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("first name cannot be null or empty", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorFirstNameEmpty() {
		try {
			new BankAccount(" ", "sgfds6", 1245, 0.0, 1998);
			fail("empty first name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("first name cannot be null or empty", ex.getMessage());
		}
		
		try {
			new BankAccount("", "456sdf", 1245, 0.0, 1998);
			fail("empty first name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("first name cannot be null or empty", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorLastNameNull() {
		try {
			new BankAccount("Mason", null, 1245, 0.0, 1998);
			fail("null last name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("last name cannot be null or empty", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorLastNameEmpty() {
		try {
			new BankAccount("Msdag", " ", 1245, 0.0, 1998);
			fail("empty last name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("last name cannot be null or empty", ex.getMessage());
		}
		
		try {
			new BankAccount("sdf7s", "", 1245, 0.0, 1998);
			fail("empty last name must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("last name cannot be null or empty", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorPinOverFourDigits() {
		try {
			new BankAccount("afd", "sdfgdf", 47352, 0.0, 1998);
			fail("pin that is not 4 digits throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("pin must be 4 digits or less", ex.getMessage());
		}
	}
	
	
	@Test
	public void testConstructorPinNegative() {
		try {
			new BankAccount("afd", "sdfgdf", -3672, 0.0, 1998);
			fail("pin that is not four digits throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("pin must be 4 digits or less", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorBadStartingBalance() {
		try {
			new BankAccount("Mason", "Liang", 0000, -1.0, 1998);
			fail("starting balance less than 0 must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("balance has to be greater than 0", ex.getMessage());
		}
	}
	
	@Test
	public void testConstructorBadYearOpened() {
		try {
			new BankAccount("Mason", "Liang", 0000, 0.0, 2219);
			fail("account year opened greater than 2018 (this year) must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("inputted year greater than current year", ex.getMessage());
		}
		
		try {
			new BankAccount("Mason", "Liang", 0000, 0.0, -1234);
			fail("negative account year opened must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("year cannot be negative", ex.getMessage());
		}
	}
	
	@Test
	public void testgetFirstName() {
		b1 = new BankAccount("Mason", "Liang", 1234, 5000.56, 2016);
		b2 = new BankAccount("JaCkie", "cheN", 7224, 5403450.43, 1988);
		
		assertEquals("Mason",  b1.getFirstName());
		assertEquals("JaCkie", b2.getFirstName());
	}
	
	
	@Test
	public void testgetLastName() {
		b1 = new BankAccount("Mason", "Liang", 1234, 5000.56, 2016);
		b2 = new BankAccount("JaCkie", "cheN", 7224, 5403450.43, 1988);
		
		assertEquals("Liang", b1.getLastName());
		assertEquals("cheN",  b2.getLastName());
	}
	
	@Test
	public void testgetPIN() {
		b1 = new BankAccount("Mason", "Liang", 1234, 5000.56, 2016);
		b2 = new BankAccount("JaCkie", "cheN", 7224, 5403450.43, 1988);
		
		assertEquals(1234,  b1.getPIN());
		assertEquals(7224,  b2.getPIN());
	}
	
	@Test
	public void testgetBalanceUSD() {
		b1 = new BankAccount("Mason", "Liang", 1234, 5000.03, 2016);
		b2 = new BankAccount("JaCkie", "cheN", 7224, 5403450.31, 1988);
		
		assertEquals(5000.03,     b1.getBalanceUSD(), 0.01);
		assertEquals(5403450.31,  b2.getBalanceUSD(), 0.01);
	}
	
	@Test
	public void testgetYearOpened() {
		b1 = new BankAccount("Mason", "Liang", 1234, 5000.56, 2016);
		b2 = new BankAccount("JaCkie", "cheN", 7224, 5403450.43, 1988);
		
		assertEquals(2016,  b1.getYearOpened());
		assertEquals(1988,  b2.getYearOpened());
	}
	
	@Test
	public void testSetBadPIN() {
		BankAccount AccountA;
		
		AccountA = new BankAccount("a", "b", 1236, 0.0, 2018);
		
		try {
			AccountA.setPIN(12356);
			fail("new pin has to be 4 digits, must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex){
			assertEquals("pin must be 4 digits or less", ex.getMessage());
		}
		
		try {
			AccountA.setPIN(-534);
			fail("new pin has to be 4 digits, must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex){
			assertEquals("pin must be 4 digits or less", ex.getMessage());
		}
		
		try {
			AccountA.setPIN(31111);
			fail("new pin has to be 4 digits, must throw IllegalArgumentException");
		}catch(IllegalArgumentException ex){
			assertEquals("pin must be 4 digits or less", ex.getMessage());
		}

	}
	
	@Test
	public void testSetPIN() {
		BankAccount AccountA = new BankAccount("a", "b", 1546, 0.0, 2018);
		BankAccount AccountB = new BankAccount("D", "c", 1234, 0.0, 1998);
		
		AccountA.setPIN(7324);
		assertEquals(7324, AccountA.getPIN());
		AccountB.setPIN(6543);
		assertEquals(6543, AccountB.getPIN());
	}
	
	@Test
	public void testWithdraw() {
		BankAccount b1 = new BankAccount("a", "b", 4414, 400.0, 2001);
		BankAccount b2 = new BankAccount("c", "d", 3456, 19388.26, 1990);
		
		b1.withdraw(200.0);
		assertEquals(200.0,  b1.getBalanceUSD(), 0.01);
		b2.withdraw(388.25);
		assertEquals(19000.01,  b2.getBalanceUSD(), 0.01);
	}
	
	@Test
	public void testBadWithdraw() {
		BankAccount b1 = new BankAccount("a", "b", 4414, 400.0, 2001);
		BankAccount b2 = new BankAccount("c", "d", 3456, 19388.99, 1990);
		
		try {
			b1.withdraw(-200);
			fail("expected IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("you cannot withdraw a negative amount", ex.getMessage());
		}
		
		try {
			b2.withdraw(0);
			fail("expected IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("you cannot withdraw nothing", ex.getMessage());
		}
	}

	@Test
	public void testDeposit() {
		BankAccount b1 = new BankAccount("a", "b", 4414, 4200.0, 2001);
		BankAccount b2 = new BankAccount("c", "d", 3456, 5000, 1990);
		
		b1.deposit(2200);
		assertEquals(6400.0,  b1.getBalanceUSD(), 0.01);
		b2.deposit(4000.25);
		assertEquals(9000.25,  b2.getBalanceUSD(), 0.01);
	}
	
	@Test
	public void testBadDeposit() {
		BankAccount b1 = new BankAccount("a", "b", 4414, 0.0, 2001);
		BankAccount b2 = new BankAccount("c", "d", 3456, 19388.99, 1990);
		
		try {
			b1.deposit(-200.0);
			fail("expected IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("you cannot deposit a negative amount", ex.getMessage());
		}
		
		try {
			b2.deposit(0.0);
			fail("expected IllegalArgumentException");
		}catch(IllegalArgumentException ex) {
			assertEquals("you cannot deposit nothing", ex.getMessage());
		}
	}
}
