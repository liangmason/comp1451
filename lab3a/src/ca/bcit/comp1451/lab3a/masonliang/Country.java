/**
 * 
 */
package ca.bcit.comp1451.lab3a.masonliang;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author Mason Liang
 *
 */
public class Country {
	private String[][] provinces;
	private HashMap<String, String> province;

	/**
	 * Country class, contains a array of provinces abreviations and name.
	 */
	public Country() {
		provinces = new String[10][2];

		provinces[0][0] = "ab";
		provinces[1][0] = "bc";
		provinces[2][0] = "mb";
		provinces[3][0] = "nb";
		provinces[4][0] = "nl";
		provinces[5][0] = "ns";
		provinces[6][0] = "on";
		provinces[7][0] = "pe";
		provinces[8][0] = "qc";
		provinces[9][0] = "sk";

		provinces[0][1] = "alberta";
		provinces[1][1] = "british columbia";
		provinces[2][1] = "manitoba";
		provinces[3][1] = "new brunswick";
		provinces[4][1] = "newfoundland";
		provinces[5][1] = "nova scotia";
		provinces[6][1] = "ontario";
		provinces[7][1] = "prince edward island";
		provinces[8][1] = "quebec";
		provinces[9][1] = "saskatchewan";

		province = new HashMap<String, String>();

	}

	/**
	 * Creates a hashmap of provinces
	 */
	public void createHashMap() {
		for (int i = 0; i < provinces.length; i++) {
			province.put(provinces[i][0], provinces[i][1]);
		}
	}

	/**
	 * shows all mappings using a entry set
	 */
	public void showAllMappings() {
		Set<Map.Entry<String, String>> set = province.entrySet();
		for (Map.Entry<String, String> w : set) {
			System.out.println("The abbreviation " + w.getKey() + " is for the province of "
					+ w.getValue().substring(0, 1).toUpperCase() + w.getValue().substring(1));
		}
	}

	/**
	 * shows all mappings using a entry set and a iterator
	 */
	public void showAllMappings2() {
		Set<Map.Entry<String, String>> set = province.entrySet();
		Iterator<Entry<String, String>> it = set.iterator();

		while (it.hasNext()) {
			Entry<String, String> s = it.next();
			System.out.println("The province " + s.getValue().substring(0, 1).toUpperCase() + s.getValue().substring(1) + " has a abbreviation of "
					+ s.getKey());
		}
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public boolean doesAnyProvinceContains(String substring) {
		createHashMap();
		for (String key : province.keySet()) {
			if (key.equalsIgnoreCase(substring)) {
				return true;
			}
		}
		for (String key : province.values()) {
			if (key.equalsIgnoreCase(substring)) {
				return true;
			}
		}
		return false;
	}

}
