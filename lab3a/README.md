# **Lab 3a**
Lab 3 is an introductory lab to hash maps using a string key and a string value. Abbreviations of a province are used as the key while the full province name is the value in the pairing.
## *Country.java ##
This file contains a class Country that creates a hash map for all provinces of Canada. It contains a function to show all keys, to show all values, and to check if a province exists within the map.
