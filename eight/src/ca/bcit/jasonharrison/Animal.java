package ca.bcit.jasonharrison;

public abstract class Animal implements Speakable, Moveable, Comparable<Animal>{

	private int weightKg;
	
	
	
	public Animal(int weightKg) {
		super();
		this.weightKg = weightKg;
	}

	@Override
	public int compareTo(Animal a) {
		if(this.weightKg == a.weightKg) {
			return 0;
		}else if(this.weightKg > a.weightKg) {
			return 1;
		}else {
			return -1;
		}
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " weighs " + weightKg;
	}


	public int getWeightKg() {
		return weightKg;
	}



	@Override
	public void foo() {
		
	}
	
}
