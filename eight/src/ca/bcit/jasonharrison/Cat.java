package ca.bcit.jasonharrison;

public class Cat extends Animal{
	
	public Cat(int kg) {
		super(kg);
	}
	
	@Override
	public void speak() {
		System.out.println("meow");
	}
	
	@Override
	public void whisper() {
		System.out.println("mew");
	}	
	
	@Override
	public void shout() {
		System.out.println("hsssssssssssss");
	}	
	
	@Override
	public void move(int speedKmPerHour) {
		System.out.println("spaz at " + speedKmPerHour + "km/hr");
	}
	

}
