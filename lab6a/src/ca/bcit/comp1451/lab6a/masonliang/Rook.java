/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Rook extends ChessPiece{

	/**
	 * @param colour white if true
	 */
	public Rook(boolean colour) {
		super(colour, 5);
	}
	
	/**
	 * rook moves horizontally or vertically
	 */
	@Override
	public void move() {
		System.out.println("horizontally or vertically");
	}
	
	/**
	 * override toString for rook
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}
}
