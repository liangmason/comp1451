/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Main {
	private static final int MAX_CHESSBOARD_SIZE = 8;
	private static final int BOTTOM_ROW_FIRST 	= 0;
	private static final int BOTTOM_ROW_SECOND 	= 1;
	private static final int TOP_ROW_FIRST 		= 7;
	private static final int TOP_ROW_SECOND 	= 6;

	private static final int ROOK_LEFT 		= 0;
	private static final int KNIGHT_LEFT 	= 1;
	private static final int BISHOP_LEFT 	= 2;
	private static final int QUEEN 			= 3;
	private static final int KING			= 4;
	private static final int BISHOP_RIGHT 	= 5;
	private static final int KNIGHT_RIGHT 	= 6;
	private static final int ROOK_RIGHT 	= 7;
	
	private static final int PAWN_1			= 0;
	private static final int PAWN_2			= 1;
	private static final int PAWN_3			= 2;
	private static final int PAWN_4			= 3;
	private static final int PAWN_5			= 4;
	private static final int PAWN_6			= 5;
	private static final int PAWN_7			= 6;
	private static final int PAWN_8			= 7;
	
	
	private static ChessPiece[][] board = new ChessPiece[MAX_CHESSBOARD_SIZE][MAX_CHESSBOARD_SIZE];

	/**
	 * makes a chessboard
	 */
	public static void main(String[] args) {
		//fills empty rows with nulls
		for(int i = 0; i < MAX_CHESSBOARD_SIZE; i++) {
			for(int j = 0; j < MAX_CHESSBOARD_SIZE; j++) {
				board[i][j] = null;
			}
		}
		
		
		// First row white
		board[BOTTOM_ROW_FIRST][ROOK_LEFT] 		= new Rook	(true);
		board[BOTTOM_ROW_FIRST][KNIGHT_LEFT] 	= new Knight(true);
		board[BOTTOM_ROW_FIRST][BISHOP_LEFT] 	= new Bishop(true);
		board[BOTTOM_ROW_FIRST][QUEEN] 			= new Queen	(true);
		board[BOTTOM_ROW_FIRST][KING] 			= new King  (true);
		board[BOTTOM_ROW_FIRST][BISHOP_RIGHT] 	= new Bishop(true);
		board[BOTTOM_ROW_FIRST][KNIGHT_RIGHT] 	= new Knight(true);
		board[BOTTOM_ROW_FIRST][ROOK_RIGHT] 	= new Rook	(true);

		// Second row white
		board[BOTTOM_ROW_SECOND][PAWN_1] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_2] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_3] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_4] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_5] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_6] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_7] = new Pawn(true);
		board[BOTTOM_ROW_SECOND][PAWN_8] = new Pawn(true);

		// First row black
		board[TOP_ROW_FIRST][ROOK_LEFT] 	= new Rook	(false);
		board[TOP_ROW_FIRST][KNIGHT_LEFT] 	= new Knight(false);
		board[TOP_ROW_FIRST][BISHOP_LEFT] 	= new Bishop(false);
		board[TOP_ROW_FIRST][QUEEN] 		= new Queen	(false);
		board[TOP_ROW_FIRST][KING] 			= new King	(false);
		board[TOP_ROW_FIRST][BISHOP_RIGHT] 	= new Bishop(false);
		board[TOP_ROW_FIRST][KNIGHT_RIGHT] 	= new Knight(false);
		board[TOP_ROW_FIRST][ROOK_RIGHT] 	= new Rook	(false);

		// Second row black
		board[TOP_ROW_SECOND][PAWN_1] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_2] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_3] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_4] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_5] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_6] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_7] = new Pawn(false);
		board[TOP_ROW_SECOND][PAWN_8] = new Pawn(false);
		
		//loops through and print out pieces
		for (int i = 0; i < MAX_CHESSBOARD_SIZE; i++) {
	        for (int j = 0; j < MAX_CHESSBOARD_SIZE; j++) {
	            System.out.print(board[i][j] + " ");
	        }
	        System.out.println();
	    }
	}	
}
