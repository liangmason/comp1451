/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Queen extends ChessPiece{

	/**
	 * @param colour white if true
	 */
	public Queen(boolean colour) {
		super(colour, 9);
	}
	
	/**
	 * queen moves like a bishop or a rook
	 */
	@Override
	public void move() {
		System.out.println("like a bishop or a rook");
	}
	
	/**
	 * override toString for queen
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}
}
