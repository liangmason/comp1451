/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class King extends ChessPiece{

	/**
	 * @param colour white if true
	 */
	public King(boolean colour) {
		super(colour, 1000);
	}
	
	/**
	 * king moves one square
	 */
	@Override
	public void move() {
		System.out.println("one square");
	}
	
	/**
	 * override toString for queen
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}
}
